const PDFDocument = require('pdfkit');
const fs = require('fs');
const readline = require('readline');
const mkdirp = require('mkdirp');
const basepath =  __dirname.replace('plugins', '');
const cfg = require('../config');
const moment = require('moment/moment');
moment.locale('id')
const archiver = require('archiver');
const curFormat = require('../plugins/currency-format')
// const changeCase = require('change-case')

const pdfArtajasa = async (doc, data) => {
// DESIGN
    const color = {
        fill1: '#8c3f4f',
        fill2: '#5e574f',
        fill3: '#808080',
    }
    // ### LINE 1
    doc
        .rect((doc.page.width - 138 - 35), 60, 138, 18)
        .fill(color.fill1)
        .stroke();
    // ### LINE 2
    doc
        .rect(25, 120, 138, 18)
        .rect(165, 120, 138, 18)
        .rect(305, 120, 138, 18)
        .rect(445, 120, 138, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 3
    doc
        .rect(25, 320, 138, 18)
        .rect(165, 320, 138, 18)
        .rect(305, 320, 138, 18)
        .rect(445, 320, 138, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 4
    doc
        .rect(25, 365, 75, 18)
        .rect(102, 365, 75, 18)
        .rect(179, 365, 277, 18)
        .rect(458, 365, 125, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 5
    doc
        .rect(25, 612, doc.page.width - 50, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 6
    doc
        .rect(25, 632, 70, 18)
        .rect(97, 632, 70, 18)
        .rect(169, 632, 70, 18)
        .rect(241, 632, 70, 18)
        .rect(313, 632, 70, 18)
        .rect(385, 632, 130, 18)
        .rect(517, 632, 70, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 7
    doc
        .rect(25, 662, 280, 18)
        .rect(307, 662, 280, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 8
    doc
        .rect(25, 682, 68.5, 18)
        .rect(95.5, 682, 68.5, 18)
        .rect(166, 682, 68.5, 18)
        .rect(236.5, 682, 68.5, 18)
        //
        .rect(307, 682, 139, 18)
        .rect(448, 682, 139, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 9
    doc
        .rect(25, 722, 90, 18)
        .fill(color.fill1)
        .stroke();

    // TEXT
    //region LINE 1
    doc
        // LINE 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Nomor Kartu Kredit Anda', (doc.page.width - 138 - 35), 62, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Your Credit Card Number', (doc.page.width - 138 - 35), 69, {
            width: 138,
            align: 'center'
        })
        //endregion LINE 1

        //region LINE 2
        // LINE 2 1
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Dicetak', 25, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Statement Date', 25, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Jatuh Tempo', 165, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Due Date', 165, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Total Tagihan Baru', 305, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Total New Balance', 305, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Total Pembayaran Minimum', 445, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Total Minimum Payment', 445, 129, {
            width: 138,
            align: 'center'
        })
    //endregion LINE 2

    //region LINE 3
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text(data.h3.tgl_dicetak, 25, 145, {
            width: 138,
            align: 'center'
        })
        .text(data.h3.tgl_jth_tmpo, 165, 145, {
            width: 138,
            align: 'center'
        })
        .text(curFormat.currencyFormat(data.h4.ttl_tagihan_baru, 'Rp '), 305, 145, {
            width: 138,
            align: 'center'
        })
        .text(curFormat.currencyFormat(parseInt(data.h4.ttl_pem_min), 'Rp '), 445, 145, {
            width: 138,
            align: 'center'
        })
    //endregion

    //region SUB HEADER
    console.log(data)
    const shMessage = 'Untuk nasabah QNB First, nikmati diskon hingga 25% di aplikasi mobile & website Zalora Indonesia menggunakan Kartu Kredit QNB First Visa Infinite s/d 14 Juni 2021. S&K berlaku. Info lebih lanjut hubungi RM Anda atau Contact Center Bank QNB Indonesia (+62 21) 30055300 Gunakan selalu aplikasi QNB Indonesia Mobile Banking (Dooet+) untuk transaksi pembayaran tagihan Kartu Kredit QNB Anda.'
    doc
        .font('arial')
        .fontSize(9)
        .text('Yth. Bapak / Ibu :', 35, 175)
        .text(data.h1.cust_name, 35, 187)
        .text(data.h2.addr1, 35, 199)
        .text(data.h2.addr2, 35, 211)
        .text(data.h2.addr3, 35, 223)
        .text(data.h2.addr4, 35, 235)
        .text('15321', 35, 247)
        .font('code_3_of_9')
        .fontSize(15)
        .text('*000146-200917-2313-2313*', 35, 257)
        .fontSize(5)
        .font('arial')
        .text('000146-2009170001-0001-', 35, 275)
        .fontSize(9)

        .text(shMessage, 360, 175, {
            width: 175,
            align: 'justify'
        })
    //endregion

    doc
        .font('arial')
        .fontSize(6)
        .text('Halaman : 1', 35, 310)

    //region LINE 4
    // LINE 4 1
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Batas Kredit', 25, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit Limit', 25, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Batas Penarikan Tunai', 165, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance Limit', 165, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Sisa Kredit', 305, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit Available', 305, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Sisa Penarikan Tunai', 445, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Available Cash Advance', 445, 329, {
            width: 138,
            align: 'center'
        })
    //endregion LINE 2

    //region LINE 5
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text(curFormat.currencyFormat(parseInt(data.h4.batas_kredit), 'Rp '), 25, 345, {
            width: 138,
            align: 'center'
        })
        .text(curFormat.currencyFormat(parseInt(data.h4.bts_pen_tun), 'Rp '), 165, 345, {
            width: 138,
            align: 'center'
        })
        .text(curFormat.currencyFormat(parseInt(data.h4.bts_pen_tun), 'Rp '), 305, 345, {
            width: 138,
            align: 'center'
        })
        .text(curFormat.currencyFormat(parseInt(data.h4.sisa_pen_tun), 'Rp '), 445, 345, {
            width: 138,
            align: 'center'
        })
    //endregion

    //region LINE 6
    // LINE 6 1
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Transaksi', 25, 367, {
            width: 75,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Transaction Date', 25, 374, {
            width: 75,
            align: 'center'
        })
        // LINE 6 2
        .font('Times-Roman')
        .text('Tanggal Pembukuan', 102, 367, {
            width: 75,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Posting Date', 102, 374, {
            width: 75,
            align: 'center'
        })
        // LINE 6 3
        .font('Times-Roman')
        .text('Uraian Transaksi Anda', 179, 367, {
            width: 277,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Your Transaction Details', 179, 374, {
            width: 277,
            align: 'center'
        })
        // LINE 6 4
        .font('Times-Roman')
        .text('Jumlah', 458, 367, {
            width: 125,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Amount', 458, 374, {
            width: 125,
            align: 'center'
        })
    //endregion LINE 2

    //region BODY
    const shMessage2 = 'Demi keamanan dan kenyamanan bertransaksi gunakan PIN Kartu Kredit Anda. Per 1 Juli 2020 semua transaksi Kartu Kredit Harus menggunakan PIN. Hindari penggunan PIN yang mudah di ketahui oleh orang lain. Untuk Permintaan PIN Kartu Kredit Anda dan informasi lebih lanjut hubungi Contact Center Bank QNB Indonesia (+62 21) 300 55 300. Kami menghimbau Anda untuk lebih berhati - hati terhadap berbagai macam jenis penipuan. Jagalah kerahasiaan data kartu kredit anda. Bank QNB Indonesia tidak pernah meminta data pribadi dan kartu kredit termasuk nomor kartu kredit masa berlaku kartu.'
    doc
        .font('arial')
        .fillColor('black')
        .fontSize(9)
        .text(shMessage2, 65, 390, {
            width: 475,
            align: 'center'
        })
    //endregion

    //region FOOTER 1
    // SUB FOOTER
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(8)
        .text('Transaksi dan Pembayaran / Transaction and Payment', 25, 617, {
            width: doc.page.width - 50,
            align: 'center'
        })
        // FOOTER 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Tagihan Sebelumnya', 25, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Previous Balance', 25, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Pembelanjaan', 97, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Retail', 97, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Traik Tunai', 169, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance', 169, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Pembayaran', 241, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Payment', 241, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 5
        .font('Times-Roman')
        .fontSize(7)
        .text('Kredit', 313, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit', 313, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 6
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga dan Biaya Administrasi', 385, 634, {
            width: 130,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Interest and Administration Fee', 385, 641, {
            width: 130,
            align: 'center'
        })
        // FOOTER 1 7
        .font('Times-Roman')
        .fontSize(7)
        .text('Tagihan Baru', 517, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('New Balance', 517, 641, {
            width: 70,
            align: 'center'
        })
    //endregion LINE 2

    //region FOOTER 2
    // SUB FOOTER 2
    doc
        // FOOTER 2 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(8)
        .text('Ringkasan Poin / Points Summary', 25, 668, {
            width: 280,
            align: 'center'
        })
        // FOOTER 2 2
        .font('Times-Roman')
        .fontSize(8)
        .text('Bunga & Total Transaksi / Interest & Total Transaction', 307, 668, {
            width: 280,
            align: 'center'
        })
    //endregion FOOTER 2

    //region FOOTER 3
    // SUB FOOTER
    doc
        // FOOTER 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Sebelemnya', 25, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Previous Points', 25, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Bulan Ini', 95.5, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Points of This Month', 95.5, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Terpakai', 166, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Adjustment Points', 166, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Tersisa', 236.5, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Points Balance', 236.5, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 5
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga Pembalanjaan (Bulanan/Tahunan)', 307, 684, {
            width: 139,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Retail Interest (Monthly/Annual)', 307, 691, {
            width: 139,
            align: 'center'
        })
        // FOOTER 1 6
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga Penarikan Tunai (Bulanan / Tahunan)', 448, 684, {
            width: 139,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance Interest (Monthly / Annual)', 448, 691, {
            width: 139,
            align: 'center'
        })
    //endregion LINE 2

    //region FOOTER 4
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text(parseInt(data.d4.poin_sblmnya), 25, 707, {
            width: 68.5,
            align: 'center'
        })
        .text(parseInt(data.d4.poin_bln_ini), 95.5, 707, {
            width: 68.5,
            align: 'center'
        })
        .text(parseInt(data.d4.poin_terpakai), 166, 707, {
            width: 68.5,
            align: 'center'
        })
        .text(parseInt(data.d4.poin_tersisa), 236.5, 707, {
            width: 68.5,
            align: 'center'
        })
        // .text('2.00 % / 24 %', 307, 707, {
        .text('2.00 % / 24 %', 307, 707, {
            width: 139,
            align: 'center'
        })
        // .text('2.00 % / 24 %', 448, 707, {
        .text('2.00 % / 24 %', 448, 707, {
            width: 139,
            align: 'center'
        })
    //endregion

    //region FOOTER 5
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Kolektibilitas', 25, 724, {
            width: 90,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Collectibility', 25, 731, {
            width: 90,
            align: 'center'
        })
        .fillColor('black')
        .font('Times-Roman')
        .fontSize(9)
        .text('Lancar', 25, 751, {
            width: 90,
            align: 'center'
        })
        .text('(Current)', 25, 761, {
            width: 90,
            align: 'center'
        })
        //
        .fontSize(7)
        .fillColor('grey')
        .text('1. Tingkat suku bunga dapat berubah sewaktu - waktu. Bacalah informasi penting tentang Kartu Kredit QNB Anda dibalik lembar ini.', 120, 726)
        .text('2. Kolektibilitas (Kualitas Kredit) yang berlaku adalah yang Terdaftar di Bank Indonesia', 120, 733)
        .text('3. Jika dalam 7 hari sebelum tanggal jatuh tempo Anda tidak menerima laporan tagihan bulanan mohon hubungi Contact Center di (+62 21) 300 55 300', 120, 740)
        //
        .font('Times-Italic')
        .text('1. Interest rate is subject to change. Please read the important information of your QNB Credit Card at the back of this page.', 120, 750)
        .text('2. The valid Collectibility (Credit Quality) is the one registered in Bank Indonesia.', 120, 757)
        .text('3. If 7 days prior to your due date you not have received billing statement please contact Contact Center on (+62 21) 300 55 300.', 120, 764)
    //endregion

};

function remove_plus_minus(data){
    if (data.includes('-')){
        data = data.replace('-', ' CR')
    }else{
        data = data.replace('+', '')
    }
    // return data.replace('0 CR', '0')
    return data
}

module.exports = {
    pdfArtajasa
}

