import {Cell as excl} from "exceljs";

const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
const moment = require('moment/moment');
const mkdirp = require('mkdirp');
const fs = require('fs');

const transformer = {};
transformer.generateRekap = (data) => generateRekap(data);
// transformer.listUserAttendWorkerBindLimit = (fromDate, toDate, userAttend) => userAttendanceListWorkerBind(fromDate, toDate, userAttend);
// transformer.listUserAttendClientBindLimit = (fromDate, toDate, userRequestLeave, userAttend) => userAttendanceListClientBind(fromDate, toDate, userRequestLeave, userAttend);
// transformer.detailClockinClockoutWorker = (companyOffice, job, shift, distance) => clockinClockoutDataDetailWorker(companyOffice, job, shift, distance);
// transformer.detailClockinClockoutClient = (user, job, shift, distance) => clockinClockoutDataDetailClient(user, job, shift, distance);
// transformer.listUserAttendActivity = (activites) => activites.map(data => userAttendActivity(data));
// transformer.detailUserAttendActivity = (activity) => detailUserAttendActivity(activity);
// transformer.userAttendActivity = (activity) => userAttendActivity(activity);
// transformer.listTaskAssigmentUser = (activites) => activites.map(data => userTaskAssignment(data));

const generateRekap = async (data) => {
    // const path = `/mnt/cdn/mega-jiwa-pos/data/output/samplexxx.xlsx`
    // let dateNow = moment().format("DD-MM-YYYY")
    let filename = `rekap-mega-jiwa.xlsx`
    let filepath = '/mnt/cdn/mega-jiwa-pos/data/output/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR With Nin';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    const headerName = ['NO', 'FILENAME', 'AMPLOP', 'KERTAS']
    let cntRow = 1;
    worksheet.getCell(cntRow, 1).value = 'Rekap Mega Jiwa POS Harian Mega';
    worksheet.getCell(cntRow, 1).style = { font: { bold: true, name: 'Arial', size: 12 }};
    cntRow++
    worksheet.getCell(cntRow, 1).value = 'Cetak 03 Agustus 2021';
    worksheet.getCell(cntRow, 1).style = { font: { bold: true, name: 'Arial', size: 12 }};
    cntRow++
    data.forEach((el, index) => {
        const row = worksheet.getRow(cntRow);
        worksheet.getRow(cntRow).values = ['no', 'filename', 'amplop', 'kertas'];
        row.eachCell({includeEmpty: true}, function (cell, colNumber) {
            // console.log('Cell ' + colNumber + ' = ' + cell.value);
            // console.log(data[idx].kur.length)
            cell.value = headerName[colNumber - 1];
            cell.style = {font: {bold: true, name: 'Arial'}};
            cell.alignment = {vertical: 'top', horizontal: 'center'};
            // cell.style = { font: { name: 'Arial', size: 15 } }
            // cell.fill = headerFill;
            cell.border = {
                //     // top: {style:'thin'},
                //     // left: {style:'thin'},
                bottom: {style: 'medium'},
                //     // right: {style:'thin'}
            }
        });
        worksheet.columns = [
            {key: 'no', width: 8},
            {key: 'filename', width: 15},
            {key: 'amplop', width: 8},
            {key: 'kertas', width: 8},
        ];
        worksheet.addRow(
            {
                no: index+1,
                filename: el.filename,
                amplop: el.amplop,
                kertas: el.kertas,
            });
    })


    await workbook.xlsx.writeFile(filepath + filename)
};


module.exports = transformer;