const nodemailer = require('nodemailer');
const handleBars = require('nodemailer-express-handlebars');
const basepath =  __dirname.replace('plugins', '');
const currencyFormat = require('../plugins/currency-format')
const moment = require('moment');
const cfg = require('../config');
// console.log(basepath+'public/templates')
// #region GENERATE PROCESS REKONSILE MERCHANT VOUCHER CODE

// SEND WITH GOOGLE ACCOUNT
exports.sendEmail = (detail) => {
    console.log(detail.email)
    let layoutTemplate = 'reset-password-member'
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.gmail.com',
        port: 587,
        requireTLS: true,
        // host: 'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        // port: '465',
        // secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'rajarepair.id@gmail.com',
            pass: '1sampai8'
            // user: 'didirumapea2@gmail.com',
            // pass: 'dewwyamelia1601'
        }
    });

    // if (detail.provider === null){
    //     // console.log('its not bank payment')
    //     layoutTemplate = 'mailing-default'
    // }

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: layoutTemplate+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));

    let mailOptions = {
        from: 'Reset Password Rajarepair'+' <rajarepair.id@gmail.com>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: [detail.email],
        subject: 'Request reset password rajarepair', // Subject line
        priority : 'high',
        important: false,
        template: layoutTemplate, // html body | file name
        context: {
            name : detail.name,
            email: detail.email,
            password: detail.password,
        },
        // attachments: [{   // filename and content type is derived from path
        //     path: cfg.assetPath+'rekonsile-merchant-voucher/'+filename
        // }]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
            //   console.log(error);
            //   res.json({
            //     success: true,
            //     data: 'send email error'
            //   });

        } else {
            // console.log(info);
            console.log('Send email invoice to '+detail.email+' done!')
            // console.log(detail.email)
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
            // res.json({
            //             success: true,
            //             msg: "send email data merchant rekosile berhasil!",
            //             data: data
            //         })
        }
    })
}

exports.sendEmailSettlement = (mData, mTemplate, mSubject, mFrom) => {
    // console.log(mData)
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', //'465',
        secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'nl',
            pass: 'Member2019'
        }
        //   host: 'smtp.zoho.com',// '202.158.93.230',// 'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        //   port: 465,// 58226, //'465',
        //   secureConnection: true, // true for 465, false for other ports
        //   secure: true,
        //   auth: {
        //     user: 'receipt@rimember.id',
        //     pass: 'Receipt20!9'
        // }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mTemplate+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));

    let mailOptions = {
        from: mFrom+' <receipt@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.email,
        subject: mSubject+' on '+moment(mData.tgl_pembelian).format('dddd, DD MMMM YYYY'), // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": mData.inv_code,
            },
        template: mTemplate, // html body | file name
        context: {
            fullname: mData.fullname,
            voucher_title: mData.promo_name,
            voucher_price: currencyFormat.currencyFormat(mData.promo_price, ''),
            invcode: mData.inv_code,
            tax: mData.tax,
            quantity: mData.quantity,
            payment_type: mData.payment_type,
            redirect_url: mData.redirect_url,
            created: mData.tgl_pembelian,
            total: currencyFormat.currencyFormat(mData.total, '')
        },
        attachments: [
            {   // filename and content type is derived from path
                path: cfg.assetPath+'receipt-voucher-pdf/'+mData.fid_user+mData.inv_code+'.pdf'
            }
        ]
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};

exports.sendEmailPending = (mData, mTemplate, mSubject, mFrom) => {
    // console.log(mData)
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', //'465',
        secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'nl',
            pass: 'Member2019'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mTemplate+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        from: mFrom+' <no-reply@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.email,
        subject: mSubject+' on '+moment(mData.tgl_pembelian).format('dddd, DD MMMM YYYY'), // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": mData.inv_code,
            },
        template: mTemplate, // html body | file name
        context: {
            fullname: mData.fullname,
            voucher_title: mData.promo_name,
            voucher_price: currencyFormat.currencyFormat(mData.promo_price, ''),
            invcode: mData.inv_code,
            tax: mData.tax,
            quantity: mData.quantity,
            payment_type: mData.payment_type,
            redirect_url: mData.redirect_url,
            created: mData.tgl_pembelian,
            total: currencyFormat.currencyFormat(mData.total, '')
        },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};

exports.sendEmailResetPassword = (to, newPass, mSubject, mTemplate, mName) => {
    // console.log(mData)
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', //'465',
        secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'nl',
            pass: 'Member2019'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mTemplate+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: to,
        subject: mSubject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'PWD'+moment().format('X'),
            },
        template: mTemplate, // html body | file name
        context: {
            username : to,
            password: newPass,
            fullname: mName
        },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};


//region SEND EMAIL PEPIPOST

// ---- RESET PASSWORD
exports.sendEmailResetPassword = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '25', // '587', //'465',
        secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'bumikitaadmiin',
            pass: 'bumikitaadmiin_e6f143be75d58fa9465dfb43df02f44f'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mData.template+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        from: 'Bumikita Reset Password'+' <security@bumikita.or.id>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'BMKT'+moment().format('X'),
            },
        template: mData.template, // html body | file name
        context: {
            username : mData.to,
            password: mData.new_pass,
            fullname: mData.name
        },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};

// ---- CONTENT ADMIN NOTIFICATION
exports.sendEmailPepipost = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', // '587', //'465', '25'
        secureConnection: false, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'bumikitaadmiin',
            pass: 'bumikitaadmiin_e6f143be75d58fa9465dfb43df02f44f'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates/bumikita',
            layoutsDir: basepath+'public/templates/bumikita',
            defaultLayout: mData.template+'.htm',
        },
        viewPath: basepath+'public/templates/bumikita', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        from: `${mData.from_aliases} <${mData.from}>`, // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'BMKT'+moment().format('X'),
            },
        template: mData.template, // html body | file name
        context: mData.context
        // context: {
        //     title : mData.title,
        //     user: mData.user,
        // },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};


// ---- SEND EMAIL EMS
exports.sendEmailEMS = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', // '587', //'465', '25'
        secureConnection: false, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'bumikitaadmiin',
            pass: 'bumikitaadmiin_e6f143be75d58fa9465dfb43df02f44f'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mData.template+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        // from: `${mData.from_aliases} <${mData.from}>`, // sender address ex : <test-sendemail@intercity.com>
        from: `${mData.from_aliases} <${mData.from}>`, // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                // "x-apiheader": 'BMKT'+moment().format('X'),
                "x-apiheader": `EMS@${mData.context.card_no}@${mData.cycle}@${mData.id}`,
            },
        template: mData.template, // html body | file name
        context: mData.context,
        // context: {
        //     title : mData.title,
        //     user: mData.user,
        // },
        attachments: [
          {   // filename and content type is derived from path
              filename: 'BM010820_000001.pdf',
              path: 'https://cdnx.bumikita.or.id/assets/files/pdf/CYC010820/BM010820_000001.pdf'
            // path: cfg.cdn+'files/pdf/cyc010820/BM010820_000001.pdf'
          }
        ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};