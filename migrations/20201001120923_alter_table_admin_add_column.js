
exports.up = function(knex) {
    return knex.schema.alterTable('admin', (table) => {
        table.integer('department_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('department')
            .onDelete('CASCADE')
            .index()
            .after('id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('admin', (table) => {
        table.dropColumns('department_id')
    })
};
