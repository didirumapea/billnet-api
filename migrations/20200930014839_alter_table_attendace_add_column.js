
exports.up = function(knex) {
    return knex.schema.alterTable('attendance', (table) => {
        table.integer('department_id')
        table.date('attendance_date')
            .after('employees_id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('attendance', (table) => {
        table.dropColumns('department_id')
    })
};
