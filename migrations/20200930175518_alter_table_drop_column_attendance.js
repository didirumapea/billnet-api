
exports.up = function(knex) {
    return knex.schema.alterTable('attendance', (table) => {
        table.dropColumns('department_id')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('attendance', (table) => {
        table.string('department_id')
    })
};
