
exports.up = function(knex) {
    return knex.schema.alterTable('employees', (table) => {
        table.string('address')
            .after('phone')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('employees', (table) => {
        table.dropColumns('address')
    })
};
