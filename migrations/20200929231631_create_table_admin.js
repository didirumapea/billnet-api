
exports.up = function(knex, Promise) {
    return knex.schema.createTable('admin', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('designation_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('designations')
            .onDelete('CASCADE')
            .index();
        table.string('name')
        table.string('email')
        table.string('password')
        table.string('image')
        table.string('phone')
        table.string('social_token')
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0)
        table.specificType('is_active', 'tinyint(1)')
            .defaultTo(0)
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('admin')
};
