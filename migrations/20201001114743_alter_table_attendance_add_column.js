
exports.up = function(knex) {
    return knex.schema.alterTable('attendance', (table) => {
        table.enum('status', ['sakit', 'izin', 'hadir'])
            .defaultTo('hadir')
            .after('break_time');
        table.string('reason')
            .after('status');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('attendance', (table) => {
        table.dropColumns('status')
        table.dropColumns('reason')
    })
};
