
exports.up = function(knex) {
    return knex.schema.alterTable('employees', (table) => {
        table.string('no_nip')
            .after('designation_id');
        table.dateTime('joining_date').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
            .after('is_active')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('employees', (table) => {
        table.dropColumns('no_nip')
        table.dropColumns('joining_date')
    })
};
