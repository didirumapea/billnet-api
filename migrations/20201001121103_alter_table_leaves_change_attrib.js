
exports.up = function(knex) {
    return knex.schema.alterTable('leaves', (table) => {
        table.integer('approved_by')
            .unsigned()
            .references('id')
            .inTable('admin')
            .onDelete('CASCADE')
            .index()
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('leaves', (table) => {
        table.string('approved_by')
            .alter()
    })
};
