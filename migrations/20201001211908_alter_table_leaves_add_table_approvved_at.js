
exports.up = function(knex) {
    return knex.schema.alterTable('leaves', (table) => {
        table.dateTime('approved_at')
            .after('is_active')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('leaves', (table) => {
        table.dropColumns('approved_at')
    })
};
