
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('leaves', (table) => {
        table.string('reason')
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('leaves', (table) => {
        table.date('reason')
            .alter();
    })
};
