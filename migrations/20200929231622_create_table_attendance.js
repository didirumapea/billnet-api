
exports.up = function(knex, Promise) {
    return knex.schema.createTable('attendance', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('employees_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('employees')
            .onDelete('CASCADE')
            .index();
        table.dateTime('check_in')
        table.dateTime('check_out')
        table.dateTime('overtime_in')
        table.dateTime('overtime_out')
        table.dateTime('working_hours')
        table.dateTime('break_time')
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0)
        table.specificType('is_active', 'tinyint(1)')
            .defaultTo(0)
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('attendance')
};
