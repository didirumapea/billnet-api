
exports.up = function(knex) {
    return knex.schema.alterTable('attendance', (table) => {
        table.enum('attendance_type', ['wfh', 'wfo'])
            .defaultTo('wfo')
            .after('employees_id');
        table.string('coordinate')
            .after('break_time')
        table.integer('overtime_approved_by')
            .unsigned()
            .references('id')
            .inTable('employees')
            .onDelete('CASCADE')
            .index()
            .after('overtime_out');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('attendance', (table) => {
        table.dropColumns('attendance_type')
        table.dropColumns('coordinate')
        table.dropColumns('overtime_approved_by')
    })
};
