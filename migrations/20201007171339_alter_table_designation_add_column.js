
exports.up = function(knex) {
    return knex.schema.alterTable('designations', (table) => {
        table.integer('job_level_id')
            .unsigned()
            .references('id')
            .inTable('job_level')
            .onDelete('CASCADE')
            .index()
            .after('department_id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('designations', (table) => {
        table.dropColumns('job_level_id')
    })
};
