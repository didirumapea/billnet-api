
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('attendance', (table) => {
        table.dateTime('check_in')
            .alter();
        table.dateTime('check_out')
            .alter();
        table.dateTime('overtime_in')
            .alter();
        table.dateTime('overtime_out')
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('attendance', (table) => {
        table.time('check_in')
            .alter();
        table.time('check_out')
            .alter();
        table.time('overtime_in')
            .alter();
        table.time('overtime_out')
            .alter();
    })
};
