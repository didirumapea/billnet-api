
exports.up = function(knex) {
    return knex.schema.alterTable('employees', (table) => {
        table.integer('remaining_leave')
            .defaultTo(0)
            .after('address')
        table.integer('amount_of_leave')
            .defaultTo(0)
            .after('remaining_leave')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('employees', (table) => {
        table.dropColumns('remaining_leave')
        table.dropColumns('amount_of_leave')
    })
};
