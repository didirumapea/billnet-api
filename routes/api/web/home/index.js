const express = require('express');
const router = express.Router();
const moment = require('moment/moment');
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const setupPaginator = require('knex-paginator');
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In SUBSCRIBER Route')
})

router.post('/add-subscriber', (req, res) => {
    // console.log(req.body)
    db.select(
        'id',
        'email',
    )
        .from('subscribers')
        .where({
            'email': req.body.email,
        })
        .then(data => {
            if (data.length === 0){
                db('subscribers')
                    .insert(req.body)
                    .then(data => {
                        res.json({
                            success: true,
                            message: 'Add subscriber success',
                            count: data,
                            // data: result,
                        });
                    })
            }else{
                res.json({
                    success: false,
                    message: "Email already exists.",
                });
            }
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Add subsciber error",
                // count: data.length,
                data: err,
            });
        });
});

module.exports = router;

