const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
// const checkAuth = require('../../../../middleware/check-auth')
const date = require('../../../../plugins/moment-date-format')
const mailing = require('../../../../plugins/mailing')
// const slugify = require('slugify')
// const changeCase = require('change-case')

router.get('/', (req, res) => {
    res.send('We Are In EMAIL Route')
})

router.get('/test-oracle/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*',
    )
        .from('TH_HISTORY_BRG')
        // .where(listWhere)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "blast email masih kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data blast email",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.data.length,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// GET SEND EMAIL
router.get('/send-email', (req, res) => {
    let name = 'Wila Sujatmoko'
    /*
        info.luvemedia@gmail.com
        wila58@gmail.com
        lupee1905@gmail.com
        palupi@kreatiffity.com
    */

    mailing.sendEmailEMS({
        template: 'ems-sample',
        to: 'info.luvemedia@gmail.com',
        // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
        // to: 'didirumapea@gmail.com',
        from_aliases: 'Email Management System',
        from: 'noreply@museumnasional.or.id',
        subject: `Electronic Statement Kartu Kredit Bank ABC ${name}`,
        context: {
            card_no: '4262-1100-5003-9298',
            user: name,
        }
    })
    res.json({
        success: true,
        message: "send email ok",

    });
});

// ---------------------------------------- WEBHOOKS EMAIL
// Delivery
router.post('/webhooks/deliver', async (req, res) =>  {
    let data = {
        email: req.body[0].EMAIL,
        card_no: req.body[0]['X-APIHEADER'].split('@')[1],
        cycle: req.body[0]['X-APIHEADER'].split('@')[2],
        id: req.body[0]['X-APIHEADER'].split('@')[3],
    }
    let q = await db.select(
        'email_status',
        // 'blast_status'
    )
        .from('tb_email_1810')
        .where(data)
    // console.log(q[0])
    console.log(`from deliver webhooks : ${req.body[0].EVENT}`)
    if (req.body[0].EVENT === 'sent' && q[0].email_status === 'process'){
        db('tb_email_1810')
            .where(data)
            .update({
                email_status: 'sent',
                blast_status: 'sent',
                deliver_at: date.utc(),
            })
            .then(data => {
                res.json({
                    success: true,
                    message: "email sent",
                    // count: data.length,
                    // data: data,
                });

            })
            .catch((err) => {
                console.log(err)
                res.json({
                    success: false,
                    message: "webhooks deliver error",
                    data: err
                });
            })
    }else if (q[0].email_status === 'sent'){
        // console.log(req.body[0])
        db('tb_email_1810')
            .where(data)
            .update({
                email_status: 'deliver',
                blast_status: 'sent',
                deliver_at: date.utc(),
            })
            .then(data => {
                res.json({
                    success: true,
                    message: "email deliver",
                    // count: data.length,
                    // data: data,
                });

            })
            .catch((err) => {
                console.log(err)
                res.json({
                    success: false,
                    message: "webhooks deliver error",
                    data: err
                });
            })
    }else{
        res.json({
            success: true,
            message: "no updated email status",
        });
    }
});
// Opened
router.post('/webhooks/opened', (req, res) =>  {
    // console.log(req.body[0])
    console.log('from opened webhooks')
    db('tb_email_1810')
        .where({
            'email': req.body[0].EMAIL,
            'card_no': req.body[0]['X-APIHEADER'].split('@')[1],
        })
        .update({
            email_status: 'opened',
            opened_at: date.utc()
        })
        .then(data => {
            res.json({
                success: true,
                message: "email opened",
                // count: data.length,
                // data: data,
            });

        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "webhooks opnened error",
                data: err
            });
        })
});

// ---------------------------------------- BLAST EMAIL
// GET LIST BLAST EMAIL
router.get('/blast-email/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        'cycle',
        'blast_status',
    )
        .count('* as total')
        .from('tb_email_1810')
        // .where(listWhere)
        .groupBy('cycle', 'blast_status')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "blast email masih kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data blast email",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.data.length,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});
// SEND BLAST EMAIL
router.get('/blast-email/cycle=:cycle', async(req, res) => {
    await db.select(
        '*'
    )
        .from('tb_email_1810')
        .where('cycle', req.params.cycle)
        // .orderBy('tb_email_1810_id', 'asc')
        .then(data => {
            data.forEach((element) => {
                // console.log(index)
                mailing.sendEmailEMS({
                    id: element.id,
                    cycle: element.cycle,
                    template: 'ems-sample',
                    to: element.email,
                    // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
                    // to: 'didirumapea@gmail.com',
                    from_aliases: 'Email Management System',
                    from: 'noreply@museumnasional.or.id',
                    subject: `Electronic Statement Kartu Kredit Bank ABC ${element.name}`,
                    context: {
                        // title: name,
                        card_no: element.card_no,
                        user: element.name,
                    }
                })
            })
            db('tb_email_1810')
                .where({
                    'cycle': req.params.cycle,
                })
                .update({
                    blast_status: 'on progress',
                    email_status: 'process',
                })
                .then(data => {
                    console.log('blast email success')
                    return null
                })
                .catch((err) => {
                    console.log('blast email error')
                    console.log(err)
                })
            // console.log(paginator)
            if (data.length === 0){
                res.json({
                    success: true,
                    message: "Email Masih Kosong.",
                    count: data.length,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success blast email",
                    data_total: data.length,
                });
            }
        });
});


// ---------------------------------------- RESEND EMAIL
// GET LIST RESEND EMAIL
router.get('/resend-email/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        'id',
        'name',
        'email',
        'card_no',
        'cycle',
        'email_status',
        'files',
        'generated_at',
        'deliver_at',
        'opened_at',
    )
        .from('tb_email_1810')
        // .where(listWhere)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "resend email masih kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data resend email",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});
// GET LIST SEARCH RESEND EMAIL
router.get('/resend-email/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
        '*',
        'id',
    )
        .from('tb_email_1810')
        .whereRaw(`CONCAT_WS('', name, cycle, card_no, email) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data email",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sortBy,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list email failed.",
                data: err,
            });
            console.log('email : '+err)
        });



});
// SEND RESEND EMAIL
router.get('/resend-email/id=:id/email=:email', async(req, res) => {
    await db.select(
        '*'
    )
        .from('tb_email_1810')
        .where({
            'id': req.params.id,
            'email': req.params.email,
        })
        .limit(1)
        // .orderBy('tb_email_1810_id', 'asc')
        .then(data => {
            mailing.sendEmailEMS({
                template: 'ems-sample',
                to: data[0].email,
                // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
                // to: 'didirumapea@gmail.com',
                from_aliases: 'Email Management System',
                from: 'noreply@museumnasional.or.id',
                subject: `Electronic Statement Kartu Kredit Bank ABC ${data[0].name}`,
                context: {
                    // title: name,
                    card_no: data[0].card_no,
                    user: data[0].name,
                }
            })
            db('tb_email_1810')
                .where({
                    'id': req.params.id,
                    'email': req.params.email,
                })
                .update({
                    blast_status: 'on progress',
                    email_status: 'sent',
                })
                .then(data => {
                    console.log('Resend email success')
                })
                .catch((err) => {
                    console.log('Resend email error')
                    console.log(err)
                });
            // console.log(paginator)
            if (data.length === 0){
                res.json({
                    success: true,
                    message: "Email Masih Kosong.",
                    count: data.length,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success resend email",
                    data_total: data.length,
                    data: data,
                });
            }
        });
});


// ---------------------------------------- REPORT EMAIL
// GET LIST REPORT EMAIL
router.get('/report-email/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        'cycle',
        'email_status',
    )
        .count('* as total')
        .from('tb_email_1810')
        // .where(listWhere)
        .groupBy('cycle', 'email_status')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(async paginator => {
            // console.log(paginator.data)
            if (paginator.data.length !== 0){
                    let result = paginator.data.reduce(function (r, a) {
                        r[a.cycle] = r[a.cycle] || [];
                        r[a.cycle].push(a);
                        return r;
                    }, Object.create(null));

                let objArr = []
                for (let [key, value] of Object.entries(result)) {
                    // console.log(`${key}: ${value}`);

                    let obj = {
                        cycle: key,
                        idle: 0,
                        process: 0,
                        deliver: 0,
                        opened: 0,
                        bounce: 0,
                        sent: 0
                    }
                    obj.grand_total = result[key].map(o => o.total).reduce((a, c) => {
                        return a + c
                    })
                    value.forEach((element, index) => {
                        // obj[element.email_status] = element.total

                        obj['idle'] =  element.email_status === 'idle' ?  element.total : obj['idle'] !== 0 ? obj['idle'] : 0
                        obj['process'] =  element.email_status === 'process' ?  element.total : obj['process'] !== 0 ? obj['process'] : 0
                        obj['deliver'] = element.email_status === 'deliver' ?  element.total : obj['deliver'] !== 0 ? obj['deliver'] : 0
                        obj['opened'] = element.email_status === 'opened' ?  element.total : obj['opened'] !== 0 ? obj['opened'] : 0
                        obj['bounce'] = element.email_status === 'bounce' ?  element.total : obj['bounce'] !== 0 ? obj['bounce'] : 0
                        obj['sent'] = element.email_status === 'sent' ?  element.total : obj['sent'] !== 0 ? obj['sent'] : 0

                    })
                    objArr.push(obj)
                    // objArr.forEach((element, index) => {
                    //     // obj[element.email_status] = element.total
                    //     objArr[index].sent = element.deliver + element.opened
                    // })
                }
                // console.log(objArr)
                res.json({
                    success: true,
                    message: "Success get data resend email",
                    data: objArr,
                });
            }else{
                res.json({
                    success: true,
                    message: "resend email masih kosong",
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        });
});

// GET LIST REPORT EMAIL BY CYCLE
router.get('/report-email/list/cycle=:cycle/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        'cycle',
        'email_status',
    )
        .count('* as total')
        .from('tb_email_1810')
        .where('cycle', 'like', `%${req.params.cycle}%`)
        .groupBy('cycle', 'email_status')
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(async paginator => {
            // console.log(paginator.data)
            if (paginator.data.length !== 0){
                let result = paginator.data.reduce(function (r, a) {
                    r[a.cycle] = r[a.cycle] || [];
                    r[a.cycle].push(a);
                    return r;
                }, Object.create(null));

                let objArr = []
                for (let [key, value] of Object.entries(result)) {
                    // console.log(`${key}: ${value}`);

                    let obj = {
                        cycle: key,
                        idle: 0,
                        process: 0,
                        deliver: 0,
                        opened: 0,
                        bounce: 0,
                        sent: 0
                    }
                    obj.grand_total = result[key].map(o => o.total).reduce((a, c) => {
                        return a + c
                    })
                    value.forEach((element, index) => {
                        // obj[element.email_status] = element.total

                        obj['idle'] =  element.email_status === 'idle' ?  element.total : obj['idle'] !== 0 ? obj['idle'] : 0
                        obj['process'] =  element.email_status === 'process' ?  element.total : obj['process'] !== 0 ? obj['process'] : 0
                        obj['deliver'] = element.email_status === 'deliver' ?  element.total : obj['deliver'] !== 0 ? obj['deliver'] : 0
                        obj['opened'] = element.email_status === 'opened' ?  element.total : obj['opened'] !== 0 ? obj['opened'] : 0
                        obj['bounce'] = element.email_status === 'bounce' ?  element.total : obj['bounce'] !== 0 ? obj['bounce'] : 0
                        obj['sent'] = element.email_status === 'sent' ?  element.total : obj['sent'] !== 0 ? obj['sent'] : 0

                    })
                    objArr.push(obj)
                    // objArr.forEach((element, index) => {
                    //     // obj[element.email_status] = element.total
                    //     objArr[index].sent = element.deliver + element.opened
                    // })
                }
                // console.log(objArr)
                res.json({
                    success: true,
                    message: "Success get data resend email",
                    count: objArr.length,
                    data: objArr,
                });
            }else{
                res.json({
                    success: true,
                    message: "resend email masih kosong",
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                success: false,
                message: 'Selected cycle is empty.',
                data: []
            });
        });
});


// UPDATE
router.post('/resend-email/update-email', (req, res) =>  {
    // db.select(
    //     'id',
    //     'name'
    // )
    //     .from('department')
    //     .where({
    //         name: req.body.name,
    //     })
    //     .andWhere('id', '<>', req.body.id)
    //     .then(data2 => {
    //         if (data2.length === 0){
                db('tb_email_1810')
                    .where('id', req.body.id)
                    .update({email: req.body.email})
                    .then(data => {
                        res.json({
                            success: true,
                            message: "Update email success.",
                            count: data.length,
                            data: data,
                        });

                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({
                            success: false,
                            message: 'update email failed',
                            data: err
                        })
                    })
        //     }else{
        //         res.json({
        //             success: false,
        //             message: 'email already exist',
        //             // count: data,
        //             // data: result,
        //         });
        //     }
        //     return null
        // })
        // .catch((err) =>{
        //     console.log(err)
        //     res.json({
        //         success: false,
        //         message: "Update email failed.",
        //         // count: data.length,
        //         data: err,
        //     });
        // });
});

// DELETE
router.delete('/delete/:id', (req, res) => {

    db('department').where({ id: req.params.id }).del().then((data) => {
        res.json({
            success: true,
            message: "Delete department success",
            data: data,
        });
    });
});

module.exports = router;