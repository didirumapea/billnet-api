const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
moment.locale('id')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const PDFDocument = require('pdfkit');
const cfg = require('../../../../config');
const fs = require('fs');
const readline = require('readline');
const mkdirp = require('mkdirp');
const layout1 = require('../../../../plugins/pdf').layout;
const func = require('../../../../plugins/pdf').func;
const init = require('../../../../plugins/pdf').init;
const excel = require('../../../../plugins/pdf').excel;
const summary = require('../../../../plugins/pdf').summary;
const zipFile = require('../../../../plugins/pdf').zipfile;
const unzipper = require('unzipper');
const path = require('path');
const master_prod = require('../../../../public/data/master_produk');
const master_kurir = require('../../../../public/data/master_kurir');
const txtToJson = require("txt-file-to-json");
const stringSimilarity = require("string-similarity");
const { promisify } = require('util');
const readdir = promisify(require('fs').readdir);
const stat = promisify(require('fs').stat);
const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
// const AdmZip = require('adm-zip');
// const extract = require('extract-zip')
// const pdf = require('../../../../plugins/pdf')
// const checkAuth = require('../../../../middleware/check-auth')
// const date = require('../../../../plugins/moment-date-format')
// const mailing = require('../../../../plugins/mailing')
// const slugify = require('slugify')
const changeCase = require('change-case');
const curFormat = require('../../../../plugins/currency-format')
const excelToJson = require('convert-excel-to-json');
const archiver = require('archiver');
// pdf.createPDF()

const outputPath = cfg.assetPath + 'billnet-files/assets/files/output/';
const inputPath = cfg.assetPath + 'bank_mega/data_master_extract/';


router.get('/', (req, res) => {
    res.send('We Are In GENERATE Route')
})

// GENERATE ARTAJASA
router.get('/arthajasa', async (req, res) => {
    const doc = new PDFDocument({
        margins : { // by default, all are 72
            top: 10,
            bottom:10,
            left: 10,
            right: 10
        }
    });
    doc.registerFont('code_3_of_9', 'public/fonts/bank-mega/Code3of9_0.ttf');
    doc.registerFont('arial', 'public/fonts/bank-mega/arial_0.ttf');
    doc.pipe(fs.createWriteStream('/mnt/cdn/billnet-files/arthajasa.pdf')); // write to PDF
    console.log(doc.page.height)
    // DESIGN
    const color = {
        fill1: '#8c3f4f',
        fill2: '#5e574f',
        fill3: '#808080',
    }
    // ### LINE 1
    doc
        .rect((doc.page.width - 138 - 35), 60, 138, 18)
        .fill(color.fill1)
        .stroke();
    // ### LINE 2
    doc
        .rect(25, 120, 138, 18)
        .rect(165, 120, 138, 18)
        .rect(305, 120, 138, 18)
        .rect(445, 120, 138, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 3
    doc
        .rect(25, 320, 138, 18)
        .rect(165, 320, 138, 18)
        .rect(305, 320, 138, 18)
        .rect(445, 320, 138, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 4
    doc
        .rect(25, 365, 75, 18)
        .rect(102, 365, 75, 18)
        .rect(179, 365, 277, 18)
        .rect(458, 365, 125, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 5
    doc
        .rect(25, 612, doc.page.width - 50, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 6
    doc
        .rect(25, 632, 70, 18)
        .rect(97, 632, 70, 18)
        .rect(169, 632, 70, 18)
        .rect(241, 632, 70, 18)
        .rect(313, 632, 70, 18)
        .rect(385, 632, 130, 18)
        .rect(517, 632, 70, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 7
    doc
        .rect(25, 662, 280, 18)
        .rect(307, 662, 280, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 8
    doc
        .rect(25, 682, 68.5, 18)
        .rect(95.5, 682, 68.5, 18)
        .rect(166, 682, 68.5, 18)
        .rect(236.5, 682, 68.5, 18)
        //
        .rect(307, 682, 139, 18)
        .rect(448, 682, 139, 18)
        .fill(color.fill2)
        .stroke();
    // ### LINE 9
    doc
        .rect(25, 722, 90, 18)
        .fill(color.fill1)
        .stroke();

    // TEXT
        //region LINE 1
    doc
        // LINE 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Nomor Kartu Kredit Anda', (doc.page.width - 138 - 35), 62, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Your Credit Card Number', (doc.page.width - 138 - 35), 69, {
            width: 138,
            align: 'center'
        })
        //endregion LINE 1

        //region LINE 2
        // LINE 2 1
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Dicetak', 25, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Statement Date', 25, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Jatuh Tempo', 165, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Due Date', 165, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Total Tagihan Baru', 305, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Total New Balance', 305, 129, {
            width: 138,
            align: 'center'
        })
        // LINE 2 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Total Pembayaran Minimum', 445, 122, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Total Minimum Payment', 445, 129, {
            width: 138,
            align: 'center'
        })
    //endregion LINE 2

        //region LINE 3
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text('16/09/2020', 25, 145, {
            width: 138,
            align: 'center'
        })
        .text('02/10/2020', 165, 145, {
            width: 138,
            align: 'center'
        })
        .text('3.629.00CR', 305, 145, {
            width: 138,
            align: 'center'
        })
        .text('0.00', 445, 145, {
            width: 138,
            align: 'center'
        })
        //endregion

    //region SUB HEADER
        const shMessage = 'Untuk nasabah QNB First, nikmati diskon hingga 25% di aplikasi mobile & website Zalora Indonesia menggunakan Kartu Kredit QNB First Visa Infinite s/d 14 Juni 2021. S&K berlaku. Info lebih lanjut hubungi RM Anda atau Contact Center Bank QNB Indonesia (+62 21) 30055300 Gunakan selalu aplikasi QNB Indonesia Mobile Banking (Dooet+) untuk transaksi pembayaran tagihan Kartu Kredit QNB Anda.'
        doc
            .font('arial')
            .fontSize(9)
            .text('Yth. Bapak / Ibu :', 35, 175)
            .text('JOHN DOE', 35, 187)
            .text('PT. ARTAJASA', 35, 199)
            .text('JL. LETNAN SUTOPO B 1-3', 35, 211)
            .text('SEKTOR KOMERSIL 3B LT 3', 35, 223)
            .text('15321', 35, 235)
            .font('code_3_of_9')
            .fontSize(15)
            .text('*000146-200917-2313-2313*', 35, 245)
            .fontSize(5)
            .font('arial')
            .text('000146-2009170001-0001-', 35, 265)
            .fontSize(9)

            .text(shMessage, 360, 175, {
                width: 175,
                align: 'justify'
            })
    //endregion

    doc
        .font('arial')
        .fontSize(6)
        .text('Halaman : 1', 35, 310)

        //region LINE 4
    // LINE 4 1
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Batas Kredit', 25, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit Limit', 25, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Batas Penarikan Tunai', 165, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance Limit', 165, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Sisa Kredit', 305, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit Available', 305, 329, {
            width: 138,
            align: 'center'
        })
        // LINE 4 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Sisa Penarikan Tunai', 445, 322, {
            width: 138,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Available Cash Advance', 445, 329, {
            width: 138,
            align: 'center'
        })
    //endregion LINE 2

        //region LINE 5
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text('1.000.000', 25, 345, {
            width: 138,
            align: 'center'
        })
        .text('1.000.000', 165, 345, {
            width: 138,
            align: 'center'
        })
        .text('1.000.000', 305, 345, {
            width: 138,
            align: 'center'
        })
        .text('1.000.000', 445, 345, {
            width: 138,
            align: 'center'
        })
    //endregion

        //region LINE 6
        // LINE 6 1
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Tanggal Transaksi', 25, 367, {
            width: 75,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Transaction Date', 25, 374, {
            width: 75,
            align: 'center'
        })
        // LINE 6 2
        .font('Times-Roman')
        .text('Tanggal Pembukuan', 102, 367, {
            width: 75,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Posting Date', 102, 374, {
            width: 75,
            align: 'center'
        })
        // LINE 6 3
        .font('Times-Roman')
        .text('Uraian Transaksi Anda', 179, 367, {
            width: 277,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Your Transaction Details', 179, 374, {
            width: 277,
            align: 'center'
        })
        // LINE 6 4
        .font('Times-Roman')
        .text('Jumlah', 458, 367, {
            width: 125,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Amount', 458, 374, {
            width: 125,
            align: 'center'
        })
    //endregion LINE 2

        //region BODY
    const shMessage2 = 'Demi keamanan dan kenyamanan bertransaksi gunakan PIN Kartu Kredit Anda. Per 1 Juli 2020 semua transaksi Kartu Kredit Harus menggunakan PIN. Hindari penggunan PIN yang mudah di ketahui oleh orang lain. Untuk Permintaan PIN Kartu Kredit Anda dan informasi lebih lanjut hubungi Contact Center Bank QNB Indonesia (+62 21) 300 55 300. Kami menghimbau Anda untuk lebih berhati - hati terhadap berbagai macam jenis penipuan. Jagalah kerahasiaan data kartu kredit anda. Bank QNB Indonesia tidak pernah meminta data pribadi dan kartu kredit termasuk nomor kartu kredit masa berlaku kartu.'
    doc
        .font('arial')
        .fillColor('black')
        .fontSize(9)
        .text(shMessage2, 65, 390, {
            width: 475,
            align: 'center'
        })
    //endregion

        //region FOOTER 1
    // SUB FOOTER
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(8)
        .text('Transaksi dan Pembayaran / Transaction and Payment', 25, 617, {
            width: doc.page.width - 50,
            align: 'center'
        })
        // FOOTER 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Tagihan Sebelumnya', 25, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Previous Balance', 25, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Pembelanjaan', 97, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Retail', 97, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Traik Tunai', 169, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance', 169, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Pembayaran', 241, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Payment', 241, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 5
        .font('Times-Roman')
        .fontSize(7)
        .text('Kredit', 313, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Credit', 313, 641, {
            width: 70,
            align: 'center'
        })
        // FOOTER 1 6
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga dan Biaya Administrasi', 385, 634, {
            width: 130,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Interest and Administration Fee', 385, 641, {
            width: 130,
            align: 'center'
        })
        // FOOTER 1 7
        .font('Times-Roman')
        .fontSize(7)
        .text('Tagihan Baru', 517, 634, {
            width: 70,
            align: 'center'
        })
        .font('Times-Italic')
        .text('New Balance', 517, 641, {
            width: 70,
            align: 'center'
        })
    //endregion LINE 2

        //region FOOTER 2
    // SUB FOOTER 2
    doc
        // FOOTER 2 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(8)
        .text('Ringkasan Poin / Points Summary', 25, 668, {
            width: 280,
            align: 'center'
        })
        // FOOTER 2 2
        .font('Times-Roman')
        .fontSize(8)
        .text('Bunga & Total Transaksi / Interest & Total Transaction', 307, 668, {
            width: 280,
            align: 'center'
        })
    //endregion FOOTER 2

        //region FOOTER 3
    // SUB FOOTER
    doc
        // FOOTER 1 1
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Sebelemnya', 25, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Previous Points', 25, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 2
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Bulan Ini', 95.5, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Points of This Month', 95.5, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 3
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Terpakai', 166, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Adjustment Points', 166, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 4
        .font('Times-Roman')
        .fontSize(7)
        .text('Poin Tersisa', 236.5, 684, {
            width: 68.5,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Points Balance', 236.5, 691, {
            width: 68.5,
            align: 'center'
        })
        // FOOTER 1 5
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga Pembalanjaan (Bulanan/Tahunan)', 307, 684, {
            width: 139,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Retail Interest (Monthly/Annual)', 307, 691, {
            width: 139,
            align: 'center'
        })
        // FOOTER 1 6
        .font('Times-Roman')
        .fontSize(7)
        .text('Bunga Penarikan Tunai (Bulanan / Tahunan)', 448, 684, {
            width: 139,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Cash Advance Interest (Monthly / Annual)', 448, 691, {
            width: 139,
            align: 'center'
        })
    //endregion LINE 2

        //region FOOTER 4
    doc
        .fillColor('black')
        .font('arial')
        .fontSize(9)
        .text('14.651', 25, 707, {
            width: 68.5,
            align: 'center'
        })
        .text('0', 95.5, 707, {
            width: 68.5,
            align: 'center'
        })
        .text('0', 166, 707, {
            width: 68.5,
            align: 'center'
        })
        .text('14.651', 236.5, 707, {
            width: 68.5,
            align: 'center'
        })
        .text('2.00 % / 24 %', 307, 707, {
            width: 139,
            align: 'center'
        })
        .text('2.00 % / 24 %', 448, 707, {
            width: 139,
            align: 'center'
        })
    //endregion

        //region FOOTER 5
    doc
        .fillColor('white')
        .font('Times-Roman')
        .fontSize(7)
        .text('Kolektibilitas', 25, 724, {
            width: 90,
            align: 'center'
        })
        .font('Times-Italic')
        .text('Collectibility', 25, 731, {
            width: 90,
            align: 'center'
        })
        .fillColor('black')
        .font('Times-Roman')
        .fontSize(9)
        .text('Lancar', 25, 751, {
            width: 90,
            align: 'center'
        })
        .text('(Current)', 25, 761, {
            width: 90,
            align: 'center'
        })
        //
        .fontSize(7)
        .fillColor('grey')
        .text('1. Tingkat suku bunga dapat berubah sewaktu - waktu. Bacalah informasi penting tentang Kartu Kredit QNB Anda dibalik lembar ini.', 120, 726)
        .text('2. Kolektibilitas (Kualitas Kredit) yang berlaku adalah yang Terdaftar di Bank Indonesia', 120, 733)
        .text('3. Jika dalam 7 hari sebelum tanggal jatuh tempo Anda tidak menerima laporan tagihan bulanan mohon hubungi Contact Center di (+62 21) 300 55 300', 120, 740)
        //
        .font('Times-Italic')
        .text('1. Interest rate is subject to change. Please read the important information of your QNB Credit Card at the back of this page.', 120, 750)
        .text('2. The valid Collectibility (Credit Quality) is the one registered in Bank Indonesia.', 120, 757)
        .text('3. If 7 days prior to your due date you not have received billing statement please contact Contact Center on (+62 21) 300 55 300.', 120, 764)
    //endregion

    doc.end();
    res.json({
        message: 'oke',
        // data: dataInit.jangan_dicetak
    });
});

// GENERATE MEGA
router.get('/mega', async (req, res) => {
    res.json({
        message: 'oke',
        // data: dataInit.jangan_dicetak
    });
    const rootPath = inputPath;
    // const masterPath = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 platinum/';
    let cntCetak = 0; // count cetak global
    let cntHold = 0; // count hold global
    const start = 'start '+ moment().format('HH:mm:ss'); // SET START TIME
    //count kurir global all product
    // fs.readdir(rootPath,  async (err, files) => {
    const files = await readdir(inputPath); // single process
    const parseDir = files.filter(x => x.includes('data billing')); // PARSING FOLDER FROM EXTRACTION
    const listingKota = [];
    // LOOP BY FOLDER PRODUCT e.g BARCA, PLATINUM, CARREFOUR FOLDER
    // parseDir.forEach(element => { // multi proses
    for await (const element of parseDir){
        const cntFileProduct = {};
        const masterPath = rootPath+element+'/';
        // fs.readdir(masterPath, async (err, files2) => { // multi process need highest ram
        const files2 = await readdir(masterPath); // READ DIRECTORY ON ROOT FOLDER
        const parseFiles = files2.filter(el => ['.xls', '.xlsx'].includes(path.extname(el).toLowerCase())); // EXTENSION FILES
        const stmtFiles = files2.filter(el => el.substring(0, 4).includes('STMT')); // STMT FILES
        const summaryFiles = files2.filter(el => el.substring(0, 4).includes('List'))[0]; // SET SUMMARY
        // region LOOPING INIT EXCEL DATA
        const dataInit = {
            jangan_dicetak: [],
            kuncian_kurir: [],
            lempar: [],
            pindah_kurir: [],
        }; // init data
        dataInit.sortasi_kurir = txtToJson({filePath: "./public/data/sortasi_kurir_update.txt"});
        // DEPENDENCIES
        for await (const file of parseFiles) {
            if (file.includes('jangan dicetak')) {
                dataInit.jangan_dicetak = await init.jangan_di_cetak(masterPath + file)
            } else if (file.includes('kuncian kurir')) {
                dataInit.kuncian_kurir = await init.kuncian_kurir(masterPath + file)
            } else if (file.includes('lempar')) {
                dataInit.lempar = await init.lempar(masterPath + file)
            } else if (file.includes('pindah kurir')) {
                dataInit.pindah_kurir = await init.pindah_kurir(masterPath + file)
            } else {
                console.log(file)
            }
            // console.log(file)
        }
        // console.log(dataInit.sortasi_kurir)
        //endregion
        // LOOPING FILE STMT
        const PDFDoc = {};
        const data = {
            fh: {
                seq_user_per_pdf: {}
            },
            ah: {},
            td: {
                tagihan_sebelumnya: '0',
                total: '0',
                payments: [], // list payments
                trans: [] // list trans
            },
            sh: {},
            am: {},
            at: {}
        };
        let cnt = 0;
        const rekap = {
            kur: [],
            total: {
                account: 0,
                pages: 0
            }
        };
        let PDFSample = {};
        excel.initExcel(stmtFiles[0].substring(7, 15)); // INITIALIZE EXCEL FILE
        for await (const [index, stmt] of stmtFiles.entries()) {
            const fileStream = fs.createReadStream(masterPath + stmt); // STMT FILE PATH
            const rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });
            // Note: we use the crlfDelay option to recognize all instances of CR LF
            // ('\r\n') in input.txt as a single line break.
            // init data
            const paymentKeys = ['PAYMENT VIA', 'PAYMENT RECEIVED'];
            // let cntUserPerPdf = 0;
            for await (const line of rl) {
                switch (line.substring(0, 2)) {
                    case 'FH':
                        data.fh.cntKurirAll = {};
                        data.fh.listKurir = master_kurir;
                        data.fh.seq_stmtfile = index;
                        data.fh.stmtfile = stmt;
                        data.fh.halaman = 0; // MENGHITUNG KESELURUHAN PAGE
                        // data.fh.seq_number_user = 0; // MENGHITUNG KESELURUHAN NASABAH
                        data.fh.seq_number_cetak = 0; // MENGHITUNG KESELURUHAN NASABAH CETAK
                        data.fh.count_pdf_file = {}; // COUNT USER PER PDF FILE BY KURIR
                        data.fh.periode = line.substring(17, 25); // 8
                        data.fh.product_code = line.substring(33, 37); // 4
                        data.fh.detail = master_prod.filter(x => x.product_code.includes(data.fh.product_code))[0];
                        Object.keys(PDFSample).length === 0 ? PDFSample = initSamplePDF(data.fh) : null;
                        excel.excel.find(x => x.name === 'CETAK').pathFile = `SCNasabah_MEGA${data.fh.periode}_${changeCase.titleCase(data.fh.detail.name)}.xlsx`;
                        excel.excel.find(x => x.name === 'HOLD').pathFile = `HOLD_MEGA${data.fh.periode}_${changeCase.titleCase(data.fh.detail.name)}.xlsx`;
                        excel.excel.find(x => x.name === 'LISTING KOTA').pathFile = `listing_kota_${data.fh.detail.name}.xlsx`;
                        excel.rekap_excel.find(x => x.name === 'REKAP').pathFile = `Rekapitulasi_Bankmega_Cyc${data.fh.periode}.xlsx`;
                        excel.rekap_excel.find(x => x.name === 'REKAP PRODUKSI').pathFile = `Rekapitulasi_Bankmega_Cyc${data.fh.periode}_Produksi.xlsx`;
                        excel.periode = data.fh.periode;
                        zipFile.periode = data.fh.periode;
                        rekap.prod = data.fh.detail.name;
                        break;
                    case 'AH':
                        data.fh.halaman++;
                        // data.fh.seq_number_user++;
                        data.ah.halaman = 1; // MENGHITUNG HALAMAN TIAP 1 USER
                        // data.ah.seq_number = line.substring(2, 8); // 6 // mengambil dari data
                        data.ah.request = '';
                        data.ah.user_barcode_number = line.substring(2, 8); // 6 // auto generate tiap user
                        data.ah.barcode_full = moment(data.fh.periode).format('DDMMYY') + moment().format('DD') + data.fh.detail.jenis + '0' + data.ah.user_barcode_number; // 6 // auto generate tiap user
                        data.ah.card_no = line.substring(8, 24); // 16
                        data.ah.isCetak = !dataInit.jangan_dicetak.some(x => x.NO_KARTU === data.ah.card_no);
                        data.ah.name = line.substring(24, 64).trim(); // 40
                        data.ah.company_name = line.substring(64, 104).trim(); // 40
                        data.ah.addr1 = line.substring(104, 144).trim(); // 40
                        data.ah.addr2 = line.substring(144, 184).trim(); // 40
                        data.ah.addr3 = line.substring(184, 224).trim(); // 40 // tidak terpakai
                        data.ah.addr4 = line.substring(224, 264).trim(); // 40
                        data.ah.addr5 = line.substring(264, 284).trim(); // 40
                        data.ah.pos_code = line.substring(284, 304).trim(); // 20
                        sortKurir(data, dataInit); // Sortasi Kurir tiap nasabah
                        requestLempar(data, dataInit);
                        data.ah.tgl_tagihan = line.substring(304, 312).trim(); // 20
                        data.ah.tgl_jatuh_tempo = line.substring(312, 320).trim(); // 20
                        data.ah.batas_credit = line.substring(320, 332).trim(); // 20
                        data.ah.sisa_credit = line.substring(332, 344).trim(); // 20
                        data.ah.tagihan_sebelumnya = line.substring(344, 357).trim(); // 20
                        data.ah.grand_total_trans = line.substring(357, 370).trim(); // 20
                        data.ah.bunga_biaya_trans = line.substring(357, 370).trim(); // 20
                        data.ah.pembayaran = line.substring(370, 383).trim(); // 20
                        data.ah.total_tagihan = line.substring(383, 396).trim(); // 20
                        data.ah.pembayaran_minimal = line.substring(396, 409).trim(); // 20
                        data.ah.reward_disesuaikan = line.substring(409, 422).trim(); // 20
                        data.ah.saldo_tarik_tunai = line.substring(422, 435).trim(); // 20
                        data.ah.batas_tarik_tunai = line.substring(435, 448).trim(); // 20
                        data.ah.rwd_bln_lalu = line.substring(448, 461).replace('+', '').trim(); // 20
                        data.ah.rwd_bln_ini = line.substring(461, 474).replace('+', '').trim(); // 20
                        data.ah.rwd_ditukar = line.substring(474, 487).replace('+', '').trim(); // 20
                        data.ah.rwd_tersedia = line.substring(487, 500).replace('+', '').trim(); // 20
                        data.ah.rwd_disesuaikan = line.substring(555, 571).replace('+', '').trim(); // 20
                        data.ah.rwd_kedaluarsa = line.substring(571, 583).replace('+', '').trim(); // 20
                        data.ah.bunga_penarikan_tunai_cl = line.substring(506, 512).trim();
                        data.ah.bunga_pembelanjaan = line.substring(500, 506).trim();
                        data.ah.bunga_penarikan_tunai = line.substring(506, 512).trim(); // 20
                        data.ah.bunga_trf_saldo = func.calc_trf_saldo({
                            val1: line.substring(512, 518).trim(),
                            val2: line.substring(518, 524).trim()
                        })  // 20
                        data.ah.bunga_trf_saldo = func.calc_trf_saldo({
                            val1: line.substring(512, 518).trim(),
                            val2: line.substring(518, 524).trim()
                        })  // 20
                        data.ah.sisa_cicilan = line.substring(582, 595).trim();
                        data.ah.kualitas_kredit = line.substring(595, 617).trim(); // 20
                        break;
                    case 'TD':
                        if (line.substring(0, 3) === 'TDP') {
                            data.td.tagihan_sebelumnya = line.substring(116, 134).trim()
                        } else if (line.substring(0, 3) === 'TDT') {
                            const list = {
                                tgl_transaksi: moment(line.substring(60, 68).trim()).format("DD/MM/YY"),
                                tgl_pembukuan: moment(line.substring(68, 76).trim()).format("DD/MM/YY"),
                                keterangan: line.substring(76, 114).trim(),
                                country: line.substring(114, 117).trim(),
                                jumlah: line.substring(117, 134).trim()
                            }
                            if (paymentKeys.some(v => line.substring(76, 114).trim().includes(v))) {
                                // console.log(list.keterangan)
                                list.keterangan = list.keterangan.replace('THANK YOU', data.ah.name)
                                data.td.payments.push(list)
                            } else {
                                data.td.trans.push(list)
                            }
                        } else if (line.substring(0, 3) === 'TDS') {
                            data.td.sub_total = line.substring(116, 134).trim()
                        } else if (line.substring(0, 3) === 'TDG') {
                            data.td.total = line.substring(116, 134).trim()
                        } else {
                            return 'nothing here'
                        }
                        break;
                    case 'SH':
                        if (line.length > 3) {
                            data.sh.message = line.substring(3, line.length).trim()
                            data.sh.message = data.sh.message.match(/.{1,40}/g)
                            data.sh.message = data.sh.message.join(' ')
                        }
                        break;
                    case 'AM':
                        data.am.message = line.substring(4, line.length).trim()
                        break;
                    case 'AT':
                        cnt++;
                        // CHECK CETAK
                        if (data.ah.isCetak) {
                            // SET LISTING KOTA
                            data.at = {
                                kota: data.fh.kurir.nfile,
                                jumlah: 1,
                                agent: data.fh.kurir.fullname
                            };
                            if (listingKota.some(x => x.kota === data.fh.kurir.nfile)){
                                listingKota.find(x => x.kota === data.fh.kurir.nfile).jumlah++
                            }else{
                                listingKota.push(data.at)
                            }
                            // console.log(data.fh.seq_user_per_pdf)
                            // console.log(Object.values(data.fh.seq_user_per_pdf).reduce(function (acc, arr) { return acc + arr; }, 0));
                            layout1.indexAllKurir[data.fh.kurir.name] !== undefined ? layout1.indexAllKurir[data.fh.kurir.name]++ : layout1.indexAllKurir[data.fh.kurir.name] = 1;// count total file pdf
                            layout1.indexAllKurir2[data.fh.kurir.aliases] !== undefined ? layout1.indexAllKurir2[data.fh.kurir.aliases]++ : layout1.indexAllKurir2[data.fh.kurir.aliases] = 1;
                            // INIT FIRST KURIR
                            if (cntFileProduct[data.fh.kurir.aliases] === undefined){
                                // data.fh.count_pdf_file[data.fh.kurir.aliases] = {
                                //     total: 0,
                                //     filename: ''
                                // };
                                cntFileProduct[data.fh.kurir.aliases] = 0;
                                // console.log(data.fh.listKurir.filter(x => x.code === data.fh.kurir.name).sort((a, b) => b.id - a.id))
                                if (data.fh.listKurir.filter(x => x.code === data.fh.kurir.name).sort((a, b) => b.id - a.id)[0].id === 0){
                                    data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).id = 1
                                }else{
                                    data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).id = (data.fh.listKurir.filter(x => x.code === data.fh.kurir.name).sort((a, b) => b.id - a.id)[0].id + 1)
                                }
                                PDFDoc[data.fh.kurir.aliases] = initPDFDoc(data.fh, data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).id); // SET PDF DOC
                                data.am.filenamePDF = data.fh.kurir.name + '_' + data.fh.detail.jenis + data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).id + '_000'; // naming file pdf
                                data.fh.listKurir.find(x => x.fullname === data.fh.kurir.fullname).filename = data.am.filenamePDF // NAMING PDF FILE
                                rekap.kur.push({
                                    name : data.fh.kurir.fullname,
                                    code : data.fh.kurir.name,
                                    filename: data.am.filenamePDF,
                                    account: 0,
                                    pages: 0,
                                    sequence: {
                                        start: layout1.indexAllKurir2[data.fh.kurir.aliases],
                                        end: 0
                                    }
                                })
                            }
                            // data.fh.seq_user_per_pdf[data.fh.kurir.aliases] >= 0 ? data.fh.seq_user_per_pdf[data.fh.kurir.aliases]++ : data.fh.seq_user_per_pdf[data.fh.kurir.aliases] = 1;// count total file pdf
                            data.fh.seq_number_cetak = ++cntCetak; // seq number all cetak
                            data.fh.count_pdf_file[data.fh.kurir.aliases] = ++cntFileProduct[data.fh.kurir.aliases]; // count all cetak product with group by kurir
                            data.fh.seq_user_per_pdf[data.fh.kurir.aliases] >= 0 ? data.fh.seq_user_per_pdf[data.fh.kurir.aliases]++ : data.fh.seq_user_per_pdf[data.fh.kurir.aliases] = 1;// count total file pdf
                            createSample(PDFSample, data);
                            setLayout(PDFDoc[data.fh.kurir.aliases], data, false); // cetak layout
                            // excel.cetakLogExcel(cntCetak, data, layout1.indexAllKurir[data.fh.kurir.name]); // cetak excel
                            excel.parseCetakLog(cntCetak, data, layout1.indexAllKurir[data.fh.kurir.name]); // cetak excel
                        } else {
                            cntHold++;
                            excel.cetakLogHold(cntHold, data);
                        }
                        data.ah = {};
                        data.td = {
                            tagihan_sebelumnya: '0',
                            total: '0',
                            payments: [], // list payments
                            trans: [] // list trans
                        };
                        data.at = {};
                        data.am.message = '';
                        data.sh.message = '';
                        console.log(cnt);
                        break;
                    case 'FT':
                        // console.log(console.log(data.fh.seq_number_cetak))
                        break;
                }
                // Each line in input.txt will be successively available here as `line`.
                // console.log(`Line from file: ${line}`);
            }
        }
        // CETAK COVER
        data.fh.rangePDF = {};
        for await (const [key, doc] of Object.entries(PDFDoc)){
            // console.log(key)
            // console.log(data.fh.seq_user_per_pdf)
            // CETAK COVER
            doc.switchToPage(0); // switch page to 1
            data.fh.rangePDF[key] = doc.bufferedPageRange(); // count pdf file
            layout1.cover(doc, data, key); // set cover on page 1
            PDFDoc[key].end(); // pdf end
            // data.fh.listKurir.find(x => x.aliases === key).total_account = data.fh.seq_user_per_pdf[key];
            const set_rekap = rekap.kur.find(x => x.name === key.replace('_', ' '));
            set_rekap.sequence.end = layout1.indexAllKurir2[key]; // SET REKAP SEQUENCE END
            set_rekap.account = data.fh.seq_user_per_pdf[key]; // SET ACCOUNT REKAP
            set_rekap.pages = doc.bufferedPageRange().start - 1; // SET PAGES REKAP
        }
        await summary.parseSummary(masterPath + summaryFiles, data.fh.detail.name, data.fh.periode); // CETAK SUMMARY
        excel.listing_kota(listingKota); // CETAK LISTING KOTA
        rekap.total.account = rekap.kur.reduce(function (acc, obj) { return acc + obj.account; }, 0); // SET TOTAL ACCOUNT REKAP
        rekap.total.pages = rekap.kur.reduce(function (acc, obj) { return acc + obj.pages; }, 0); // SET TOTAL PAGES REKAP
        // IF SAMPLE ACCOUNT TIDAK SAMPAI 10
        if (rekap.total.account < 10) {
            PDFSample.switchToPage(0); // switch page to 1
            layout1.coverSample(PDFSample, data); // set cover on page 1
            PDFSample.end();
        }
        cntCetak !== 0 ? excel.det_rekap.push(rekap) : null;
        // await excel.stopCetak(); // STOP CETAK EXCEL
        await excel.cetakLogExcel(); // START CETAK EXCEL
        await excel.stopCetak(); // STOP CETAK EXCEL
        data.fh = {}; // RESET FH
        master_kurir.forEach(el => { el.id = 0 }); // RESET KURIR
        // CLEAR
        // console.log(listingKota.reduce(function (acc, obj) { return acc + obj.jumlah; }, 0))
        cnt = 0;
        // END OF PRODUCT
    } // end for await parseDir
    await excel.rekap(); // CETAK REKAP EXCEL
    await excel.rekap_produksi(); // CETAK REKAP PRODUKSI EXCEL
    await summary.cetakSummary();
    await zipFile.zipCycle();
    // await summary.cetakLogSJ(excel.det_rekap);


    console.log(start);
    console.log('stop '+ moment().format('HH:mm:ss'))
    // }) foreach parseDir
    // });
});

// SORTASI MANUAL
router.get('/sortasi-manual/periode=:periode', async (req, res) => {

    const periode = req.params.periode
    const log_cetak = `/mnt/cdn/billnet-files/assets/files/output/${periode}/LOG_NASABAH_CETAK/`;
    const rekap_path = `/mnt/cdn/billnet-files/assets/files/output/${periode}/`;
    const data = {};

    fs.readdir(rekap_path,  async (err, files) => {
        // const parseFiles = files.filter(el => ['.xls', '.xlsx'].includes(path.extname(el).toLowerCase()))
        // const stmtFiles = files.filter(el => !['.xls', '.xlsx', '.txt'].includes(path.extname(el).toLowerCase()))

        // console.log(files, err);
        const fileExcelProduksi = files.find(x => x.toLowerCase().includes('produksi'));
        const sortasiDir = files.find(x => x.toLowerCase().includes('sortasi'));
        const t = []

        // console.log(fileExcelProduksi, err);
        fs.readdir(rekap_path+sortasiDir,  async (err1, files1) => {
            for (const el of files1) {
                if (el.toLowerCase().includes('sortasi')){
                    const data = excelToJson({
                        sourceFile: rekap_path+sortasiDir+'/'+el,
                        // sheets:[{
                        //     name: 'card_no',
                        // }],
                        columnToKey: {
                            A: 'NO',
                            B: 'NAMA',
                            C: 'NO_SEQ',
                            D: 'ASAL',
                            E: 'MENJADI',
                        },
                        header: {
                            // Is the number of rows that will be skipped and will not be present at our result object. Counting from top to bottom
                            rows: 4 // 2, 3, 4, etc.
                        }
                    })
                    // console.log(data[Object.keys(data)[0]])
                    // return data[Object.keys(data)[0]]
                    // console.log(periode)
                    // console.log(moment(periode).format('DDMMYY'))
                    const prod = {
                        product: el.replace(`Sortasi Manual cyc ${moment(periode).format('DDMMYY')} `, '').split('.')[0].toLowerCase().replace('travel card', 'travel').replace('regular', 'reguler'),
                        data: data[Object.keys(data)[0]]
                    }
                    t.push(prod)
                }
            }
        })

            // console.log(t)
        fs.readdir(log_cetak,  async (err2, files2) => {
                // console.log(files2.filter(x => x.toLowerCase().includes('travel')))
                const list_sortasi = [];
                const workbook = new ExcelJS.Workbook();
                for (const element of t) {
                    let data = {};
                    // console.log(element.data)
                    await workbook.xlsx.readFile(log_cetak+files2.filter(x => x.toLowerCase().includes(element.product))[0])
                        .then( function() {
                            const worksheet = workbook.worksheets[0]
                            for (const ele of element.data) {
                                data.produk = element.product.toUpperCase()
                                data.nama = ele.NAMA;
                                data.no_seq = ele.NO_SEQ;
                                data.asal = ele.ASAL;
                                data.menjadi = ele.MENJADI;
                                // console.log(ele)
                                worksheet.eachRow(function (row, rowNumber) {
                                    // console.log(ele.NO_SEQ, row.values[1])
                                    if (rowNumber > 1 && ele.NO_SEQ === row.values[1]) {
                                        data.filename = row.values[26]
                                        data.nofile = row.values[27]
                                        // console.log(row.values)
                                        // console.log(row.values[1])
                                        // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                                        // console.log(Object.keys(row.values).length)
                                        // console.log(tagihan_bpjs_kes.find(x => x.name === row.values[2]))
                                    }
                                });
                                list_sortasi.push(data)
                                data = {}
                                // console.log(data)
                            }
                        })
                    // console.log(el.product)
                }
                // console.log(list_sortasi)
                workbook.xlsx.readFile(rekap_path+`Rekapitulasi_Bankmega_Cyc${periode}_Produksi.xlsx`)
                    .then(function() {
                        const headerFill = {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: {argb: 'C0C0C0'}
                        };
                        const worksheet = workbook.worksheets[0]
                        let cntRow = worksheet.rowCount
                        const spaceRow = 3
                        // console.log(worksheet.rowCount)
                        // const row = worksheet.getRow(51);
                        worksheet.getCell(cntRow+2, 1).value = 'sortasi'; // A5's value set to 5
                        worksheet.getRow(cntRow+spaceRow).values = ['NO', 'PRODUK', 'NAMA', 'NO SEQ', 'NAMAFILE', 'NOFILE', 'ASAL', 'MENJADI'];
                        worksheet.getRow(cntRow+spaceRow).fill = headerFill
                        worksheet.getRow(cntRow+spaceRow).border = {
                                top: {style:'thin'}, // thin, medium
                                left: {style:'thin'},
                            bottom: {style: 'thin'},
                                right: {style:'thin'}
                        }
                        worksheet.getRow(cntRow+spaceRow).style = {font: {bold: true, name: 'Arial'}};

                        list_sortasi.forEach((elem, idx) => {
                            // const row = worksheet.getRow(51+idx);
                            // worksheet.getCell(cntRow+2+idx, 2).value = 5; // A5's value set to 5

                            worksheet.getRow(cntRow+4+idx).border = {
                                top: {style:'thin'}, // thin, medium
                                left: {style:'thin'},
                                bottom: {style: 'thin'},
                                right: {style:'thin'}
                            }
                            worksheet.getRow(cntRow+4+idx).values = [idx+1, elem.produk, elem.nama, elem.no_seq, elem.filename, elem.nofile, elem.asal, elem.menjadi];
                            worksheet.getRow(cntRow+4+idx).style = {font: {name: 'Arial'}};
                        })
                        // worksheet.commit();
                        // row.getCell(1).value = 5; // A5's value set to 5

                        return workbook.xlsx.writeFile(rekap_path+fileExcelProduksi);
                        // worksheet.eachRow(function (row, rowNumber) {
                        //     // console.log(ele.NO_SEQ, row.values[1])
                        //     if (rowNumber > 1 && ele.NO_SEQ === row.values[1]) {
                        //         data.filename = row.values[26]
                        //         // console.log(row.values[1])
                        //         // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                        //         // console.log(Object.keys(row.values).length)
                        //         // console.log(tagihan_bpjs_kes.find(x => x.name === row.values[2]))
                        //     }
                        // });
                    })
            });
        res.json({
            success: true,
            message: "EXCEL TO JSON ok",
            // length: data.length,
            data: data,
            // d: master_prod
        });
    });
});

// CREATE LOG SURAT JALAN
router.get('/log-surat-jalan/periode=:periode', async (req, res) => {

    const rekap_path = `/mnt/cdn/billnet-files/assets/files/output/${req.params.periode}/`;
    const data = {};

    fs.readdir(rekap_path,  async (err, files) => {
        // console.log(files, err);

        const parseFiles = files.find(x => x.toLowerCase().includes('produksi'));
        console.log(parseFiles)

        const workbook = new ExcelJS.Workbook();
        await workbook.xlsx.readFile(rekap_path+parseFiles)
            .then( async function() {
                const sum_prod = {
                    SAP: [],
                    NCS: [],
                    POS: [],
                    CARD: []
                }
                const data = {
                    NCS: [],
                    POS: [],
                    SAP: [],
                    CARD: []
                }
                let rr = null
                let dataSort = []
                const worksheet = workbook.worksheets[0]
                worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
                        if (rr !== null){
                            console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                            dataSort.push({
                                produk: row.values[2],
                                namafile: row.values[5],
                                asal: row.values[7],
                                menjadi: row.values[8]
                            })
                        }
                        if (row.values[1] === 'NO'){
                            rr = rowNumber
                        }
                        if (rowNumber > 3) {
                            if (['PT POS', 'POS'].includes(row.values[1])){
                                if (sum_prod.POS.length === 0 || sum_prod.POS.filter(x => x.code !== row.values[2].substring(0, 4)).length > 0){
                                    // sum_prod.POS.push(summary_prod({
                                    //     filename: row.values[2].substring(0, 4),
                                    //     ttl: row.values[3],
                                    // }))
                                    sum_prod.POS.push({
                                        code: row.values[2].substring(0, 4),
                                        ttl: row.values[3],
                                    })
                                }else{
                                    sum_prod.POS.find(x => x.code === row.values[2].substring(0, 4)).ttl += row.values[3]
                                }
                                data.POS.push({
                                    filename: row.values[2],
                                    ttl_acc: row.values[3]
                                })
                            }else if (['PT NCS', 'NCS'].includes(row.values[1])){
                                if (sum_prod.NCS.length === 0 || sum_prod.NCS.filter(x => x.code !== row.values[2].substring(0, 4)).length > 0){
                                    // sum_prod.NCS.push(summary_prod({
                                    //     filename: row.values[2].substring(0, 4),
                                    //     ttl: row.values[3],
                                    // }))
                                    sum_prod.NCS.push({
                                        code: row.values[2].substring(0, 4),
                                        ttl: row.values[3],
                                    })
                                }else{
                                    sum_prod.NCS.find(x => x.code === row.values[2].substring(0, 4)).ttl += row.values[3]
                                }
                                data.NCS.push({
                                    filename: row.values[2],
                                    ttl_acc: row.values[3]
                                })
                            }else if (['PT SAP', 'SAP'].includes(row.values[1])){
                                if (sum_prod.SAP.length === 0 || sum_prod.SAP.filter(x => x.code !== row.values[2].substring(0, 4)).length > 0){
                                    // sum_prod.SAP.push(summary_prod({
                                    //     filename: row.values[2].substring(0, 4),
                                    //     ttl: row.values[3],
                                    // }))
                                    sum_prod.SAP.push({
                                        code: row.values[2].substring(0, 4),
                                        ttl: row.values[3],
                                    })
                                }else{
                                    sum_prod.SAP.find(x => x.code === row.values[2].substring(0, 4)).ttl += row.values[3]
                                }
                                data.SAP.push({
                                    filename: row.values[2],
                                    ttl_acc: row.values[3]
                                })
                            }else if (['CARD CENTER', 'CARD'].includes(row.values[1])){
                                if (sum_prod.CARD.length === 0 || sum_prod.CARD.filter(x => x.code !== row.values[2].substring(0, 4)).length > 0){
                                    // sum_prod.CARD.push(summary_prod({
                                    //     filename: row.values[2].substring(0, 4),
                                    //     ttl: row.values[3],
                                    // }))
                                    sum_prod.CARD.push({
                                        code: row.values[2].substring(0, 4),
                                        ttl: row.values[3],
                                    })
                                }else{
                                    sum_prod.CARD.find(x => x.code === row.values[2].substring(0, 4)).ttl += row.values[3]
                                }
                                data.CARD.push({
                                    filename: row.values[2],
                                    ttl_acc: row.values[3]
                                })
                            }
                        }
                    });
                // const resDataSort = dataSort.reduce(function (r, a) {
                //     r[a.namafile] = r[a.namafile] || [];
                //     r[a.namafile].push(a);
                //     return r;
                // }, Object.create(null));

                // for (const [key, val] of Object.entries(resDataSort)){
                //     // console.log(val)
                //     dataSort[key] = val.reduce(function (r, a) {
                //         r[a.namafile] = r[a.namafile] || [];
                //         r[a.namafile].push(a);
                //         return r;
                //     }, Object.create(null));
                // }
                // console.log(dataSort)
                const resDataSort = []
                dataSort.forEach(res => {
                    if (resDataSort.filter(x => x.penambah.namafile === res.namafile).length === 0){
                        resDataSort.push({
                            penambah: {
                                namafile: res.namafile,
                                ttl: 1,
                                produk: {
                                    name: res.produk,
                                    ttl: 1
                                },
                                kurir: res.menjadi.replace('PT POS', 'POS').replace('NUSA', 'NCS').replace('PT SAP', 'SAP')
                            },
                            pengurang: {
                                namafile: res.namafile,
                                ttl: 1,
                                produk: {
                                    name: res.produk,
                                    ttl: 1
                                },
                                kurir: res.asal.replace('PT POS', 'POS').replace('NUSA', 'NCS').replace('PT SAP', 'SAP')
                            }
                        })
                    }else{
                        resDataSort.find(x => x.penambah.namafile === res.namafile).penambah.ttl++
                        resDataSort.find(x => x.penambah.namafile === res.namafile).pengurang.ttl++
                        resDataSort.find(x => x.penambah.produk.name === res.produk).penambah.produk.ttl++
                        resDataSort.find(x => x.penambah.produk.name === res.produk).pengurang.produk.ttl++

                        // console.log(resDataSort.find(x => x.penambah.namafile === res.namafile))
                    }
                })

                // console.log(resDataSort)
                    // console.log(sum_prod)
                    // console.log(data)
                // console.log(sum_prod)
                    const periode = parseFiles.split('_')[2].toLowerCase().replace('cyc', '');
                    for await (const [key, val] of Object.entries(data)){
                        const path = outputPath + periode + `/LOG/`
                        const file = outputPath + periode + `/LOG/LOG_SJ_${key}_MEGA_${periode}.TXT`
                        const c4File = outputPath + periode + `/LOG/LOG_SJ_${key}_MEGA_${periode}_c4.TXT`
                        if (!fs.existsSync(path)) {
                            console.log('dir not exist and will be created...');
                            const made = mkdirp.sync(path);
                            console.log(`made directories, starting with ${made}`);
                            console.log('done create directory');
                        }
                        const loggerc4 = fs.createWriteStream(c4File, {
                            flags: 'a' // 'a' means appending (old data will be preserved)
                        })
                        const logger = fs.createWriteStream(file, {
                            flags: 'a' // 'a' means appending (old data will be preserved)
                        })
                        resDataSort.forEach(el => {
                            // console.log(el.penambah.produk)
                            if (el.penambah.kurir === key){
                                logger.write(''.padEnd(21)+`${el.penambah.namafile}${el.penambah.ttl}\n`)
                            }
                        })
                        val.forEach(el => {
                            resDataSort.forEach(el1 => {
                                // console.log(el1)
                                if (el1.pengurang.kurir === key){
                                    if (el.filename === el1.pengurang.namafile){
                                        el.ttl_acc -= el1.pengurang.ttl
                                    }
                                }
                            })
                            // PEMISAH FILENAME C4
                            if (el.filename.includes('_C')){
                                loggerc4.write(''.padEnd(21)+`${el.filename}${el.ttl_acc}\n`)
                            }else{
                                logger.write(''.padEnd(21)+`${el.filename}${el.ttl_acc}\n`)
                            }
                        })
                        logger.write('99SUMMARY\n');
                        logger.write('99--------------------\n');
                        loggerc4.write('99SUMMARY\n');
                        loggerc4.write('99--------------------\n');

                        for (let i = 0; i < sum_prod[key].length; i++){
                            if (i > 0){
                                if (sum_prod[key][i].code === sum_prod[key][i-1].code){
                                    sum_prod[key][i].ttl += sum_prod[key][i-1].ttl
                                    // console.log(sum_prod[key][i])
                                    // parses[key].push(sum_prod[key][i])
                                    sum_prod[key].splice(i-1, 1)
                                    // test.push(sum_prod[key][i])
                                }
                            }
                        }
                        // console.log(parses[key])
                        sum_prod[key].forEach(el => {
                            const prod_name = master_prod.find(x => x.jenis === el.code.split('_')[1]).name.toUpperCase()
                            // console.log(master_prod.find(x => x.jenis === el.code.split('_')[1]).name)
                            // logger.write(`${prod_name.padEnd(5, '-')} = ${el.ttl}\n`);
                            // logger.write((prod_name + ' ').padEnd(13, '-') + ` = [${el.ttl}]\n`);
                            // console.log(el)
                            resDataSort.forEach(el1 => {
                                if (el1.penambah.kurir === key){
                                    if (prod_name === el1.penambah.produk.name){
                                        el.ttl += el1.penambah.produk.ttl
                                    }
                                }
                                if (el1.pengurang.kurir === key){
                                    if (prod_name === el1.pengurang.produk.name){
                                        el.ttl -= el1.pengurang.produk.ttl
                                    }
                                }
                            })
                            if (prod_name === 'CARREFOUR'){
                                loggerc4.write('99' +(prod_name + ' ').padEnd(13, '-') + ` = [${el.ttl}]\n`);
                            }else{
                                logger.write('99' + (prod_name + ' ').padEnd(13, '-') + ` = [${el.ttl}]\n`);
                            }

                            // logger.write(el.ttl+ `\n`)
                        })
                        // console.log(key, val)
                        // console.log(parseFiles.split('_')[2].toLowerCase().replace('cyc', ''))
                    }
                })

        res.json({
            success: true,
            message: "LOG SURAT JALAN ok",
            // length: data.length,
            data: data,
            // d: master_prod
        });
    });
});

// SIMILIARITY CHECK
router.get('/similiarity-check', async (req, res) => {

    const files = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 160221 regular/ListCounter20210216.TXT'; // single process

    const fileStream = fs.createReadStream(files); // STMT FILE PATH
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    const summary = {
        product : 'REGULER',
        list: []
    }

    for await (const line of rl) {
        const data = {}
        if (line.substring(0, 4) === 'STMT') {
            data.filename = line.substring(0, 25).trim();
            data.total = line.substring(71, 81).trim();
            summary.list.push(data)
        }
        // console.log(line)
    }

    const result = [
        {
            "product": "REGULER",
            "list": [
                {
                    "filename": "STMT10020210216.05072710",
                    "total": "9353"
                },
                {
                    "filename": "STMT11020210216.05072961",
                    "total": "27271"
                },
                {
                    "filename": "STMT15020210216.05073965",
                    "total": "111"
                },
                {
                    "filename": "STMT22020210216.05074200",
                    "total": "18"
                },
                {
                    "filename": "STMT30020210216.05075440",
                    "total": "68"
                }
            ]
        },
        {
            "product": "BARCA",
            "list": [
                {
                    "filename": "STMT10020210216.05072710",
                    "total": "9353"
                },
                {
                    "filename": "STMT11020210216.05072961",
                    "total": "27271"
                },
                {
                    "filename": "STMT15020210216.05073965",
                    "total": "111"
                },
                {
                    "filename": "STMT22020210216.05074200",
                    "total": "18"
                },
                {
                    "filename": "STMT30020210216.05075440",
                    "total": "68"
                }
            ]
        },
        {
            "product": "PLATINUM",
            "list": [
                {
                    "filename": "STMT10020210216.05072710",
                    "total": "9353"
                },
                {
                    "filename": "STMT11020210216.05072961",
                    "total": "27271"
                },
                {
                    "filename": "STMT15020210216.05073965",
                    "total": "111"
                },
                {
                    "filename": "STMT22020210216.05074200",
                    "total": "18"
                },
                {
                    "filename": "STMT30020210216.05075440",
                    "total": "68"
                }
            ]
        }
    ]

    const logger = fs.createWriteStream('/mnt/cdn/bank_mega/data_master_extract/data billing cyc 160221 regular/summary_stfile.TXT', {
        flags: 'a' // 'a' means appending (old data will be preserved)
    })

    result.forEach(el => {
        console.log(el.product)
        el.list.forEach(el2 => {
            logger.write(`${el2.filename};${el2.total};${el.product}\n`)
        })
    })
    console.log('end')
    await logger.end();


    // logger.write('some data') // append string to your file
    // logger.write('more data') // again
    // logger.write('and more') // again
    // logger.end();


    res.json({
        message: '',
        data: summary
    })
    // const data = [ { id: 1,
    //     fullname: 'PT POS',
    //     aliases: 'PT_POS',
    //     filename: 'PI_P1_000',
    //     code: 'PI' },
    //     { id: 0,
    //         fullname: 'POS',
    //         aliases: 'POS',
    //         filename: '',
    //         code: 'PI' },
    //     { id: 0,
    //         fullname: 'NUSA',
    //         aliases: 'NUSA',
    //         filename: '',
    //         code: 'NS' },
    //     { id: 1,
    //         fullname: 'NCS',
    //         aliases: 'NCS',
    //         filename: 'NS_P1_000',
    //         code: 'NS' },
    //     { id: 1,
    //         fullname: 'PT SAP',
    //         aliases: 'PT_SAP',
    //         filename: 'SA_P1_000',
    //         code: 'SA' },
    //     { id: 0,
    //         fullname: 'SAP',
    //         aliases: 'SAP',
    //         filename: 'SA_P1_000',
    //         code: 'SA' } ]
    // console.log(data.filter(x => x.code === 'SA').sort((a, b) => b.id - a.id)[0].id)
    // console.log(data.filter(x => x.code === 'SA').sort().reverse())

    // class Screen {
    //     constructor(width, height) {
    //         this.sHeight = height;
    //         this.sWidth = width
    //     }
    //     diagonal() {
    //         return Math.sqrt(Math.pow(this.width, 2) + Math.pow(this.height, 2));
    //     }
    //
    //     dimensions(definition) {
    //         const dimensions = definition.split('x')
    //         this.width = parseInt(dimensions[0]);
    //         this.height = parseInt(dimensions[1]);
    //     }
    // }
    //
    // const screen = new Screen(0, 0);
    // screen.dimensions = '500x300';
    // screen.width = 400;
    // console.log(screen.diagonal()); // Should print 500





    // function sortByPriceAscending(jsonString) {
    //     // Your code goes here
    //     const parseJson = JSON.parse(jsonString)
    //     return JSON.stringify(parseJson.sort( compare ))
    // }
    //
    // function compare( a, b ) {
    //     if ( a.price < b.price ){
    //         return -1;
    //     }
    //     if ( a.price > b.price ){
    //         return 1;
    //     }
    //     if (a.price === b.price){
    //         if ( a.name < b.name ){
    //             return -1;
    //         }
    //         if ( a.name > b.name ){
    //             return 1;
    //         }
    //     }
    //     return 0;
    // }
    //
    // console.log(sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04},{"name":"chicken","price":4.04},{"name":"plant","price":4.04},{"name":"flower","price":4.04}]'));




    // function findAllHobbyists(hobby, hobbies) {
    //
    //     for (const [key, value] of Object.entries(hobbies)) {
    //         if (value.filter(val => val === hobby).length !== 0){
    //             return key
    //         }
    //         // console.log(value.filter(val => val === hobby))
    //         // console.log(key, value)
    //     }
    //
    // }
    //
    // const hobbies = {
    //     "Steve": ['Fashion', 'Piano', 'Reading'],
    //     "Patty": ['Drama', 'Magic', 'Pets'],
    //     "Chad": ['Puzzles', 'Pets', 'Yoga']
    // };
    //
    // console.log(findAllHobbyists('Pets', hobbies));

    // function average(a, b) {
    //     return (a / b) * 2;
    // }
    //
    // console.log(average(2, 1));

    // const stringSimilarity = require("string-similarity");
    // const similarity = stringSimilarity.compareTwoStrings("YOGYAKARTA", "CIMAHI");
    // const txtToJson = require("txt-file-to-json");
    // const dataInJSON = txtToJson({ filePath: "./public/data/sortasi_kurir_update.txt" });
    // res.json({
    //     success: true,
    //     message: "txt to json ok",
    //     // length: result[Object.keys(result)[0]].length,
    //     data: similarity
    //     // d: master_prod
    // });

});

// PERHITUNGAN PTKP
router.get('/pph21/ptkp=:ptkp', async (req, res) => {

    // let ptkp = 260000000; //
    // PERHITUNGAN PKP
    let ptkp = req.params.ptkp; //

    const data_ptkp = [
        {
            title: 't1',
            range: '0 - 50jt',
            percentage: 0.05,
            value: 50000000,
            start: 0,
            end : 50000000,
        },
        {
            title: 't2',
            range: '50jt - 250jt',
            percentage: 0.15,
            value: 200000000,
            start: 50000001,
            end : 200000000,
        },
        {
            title: 't3',
            range: '250jt - 500jt',
            percentage: 0.25,
            value: 250000000,
            start: 250000001,
            end : 500000000,
        },
        {
            title: 't4',
            range: '500jt ++++',
            percentage: 0.3,
            value: 500000000,
            start: 500000001,
            end : 'EON',
        }
    ]
    let result_ptkp = 0;
    let total_pph21_setahun = 0;
    let sisa_ptkp = ptkp;

    for (let el of data_ptkp) {
        // console.log('sisa ptkp : '+ sisa_ptkp)
        if (sisa_ptkp > el.value){
            result_ptkp = el.value
            sisa_ptkp -= el.value;
        }
        else{
            result_ptkp = sisa_ptkp
            sisa_ptkp = sisa_ptkp - el.value < 0 ? 0 : sisa_ptkp -= el.value
        }
        // result_ptkp = el.value > ptkp ? sisa_ptkp : sisa_ptkp - el.value
        total_pph21_setahun += result_ptkp * el.percentage;
        // console.log('total nilai pph21 setahun : '+ total_pph21_setahun)
        if (el.start > ptkp && (el.end < ptkp || el.end === 'EON')){
            break;
        }
    }

    // PERHITUNGAN PTKP

    // console.log(files)
    res.json({
        success: true,
        message: "pph21 ok",
        pajak_pph21_setahun: curFormat.currencyFormat(total_pph21_setahun, 'Rp ')
    });
});
// TXT TO JSON
router.get('/txt-to-json', async (req, res) => {

    // const txtToJson = require("txt-file-to-json");
    // const dataInJSON = txtToJson({ filePath: "./public/data/sortasi_kurir_update.txt" });
    // console.log(master_prod.filter(x => x.name === 'PI'))
    // console.log(master_kurir)
    console.log(master_kurir);
    // let files = await readdir('/mnt/cdn/bank_mega/data_master_extract/')

    // for await (const element of files){
    //     console.log(element)
    //     for (let x = 0; x < 500000000; x++){
    //
    //     }
    // }


    // console.log(files)
    res.json({
        success: true,
        message: "txt to json ok",
        // length: result[Object.keys(result)[0]].length,
        // data: dataInJSON
        // d: master_prod
    });

});

// EXCEL TO JSON
router.get('/excel-to-json', async (req, res) => {

    const path1 = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 barca/data jangan dicetak cyc 190121 barca.xlsx'
    const path2 = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 barca/data kuncian kurir cyc 190121 barca.xls'
    const path3 = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 barca/data lempar cyc 190121 barca.xls'
    const path4 = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 barca/data pindah kurir cyc 190121 barca.xls'
    // const result = await init.jangan_di_cetak(path)
    // const result = await init.kuncian_kurir(path)
    // const result = await init.lempar(path)
    // const result = await init.pindah_kurir(path)
    // console.log(Object.keys(result)[0])
    res.json({
        success: true,
        message: "read excel ok",
        // length: result[Object.keys(result)[0]].length,
        // data: result[Object.keys(result)[0]],
        jgn_di_cetak: await init.jangan_di_cetak(path1),
        kuncian_kurir: await init.kuncian_kurir(path2),
        lempar: await init.lempar(path3),
        pindah_kurir: await init.pindah_kurir(path4)
        // d: master_prod
    });

});

// EXCEL TO JSON
router.get('/write-excel', async (req, res) => {

    console.log(moment('20210119').format('DD MMMM YYYY').toUpperCase())

    const data = [
        {
            prod: 'BARCA',
            kur: [
                { name: 'PT POS',
                    code: 'PI',
                    account: 150,
                    pages: 152,
                    sequence: { start: 1, end: 150 } },
                { name: 'SAP',
                    code: 'SA',
                    account: 36,
                    pages: 36,
                    sequence: { start: 1, end: 36 } },
                { name: 'NCS',
                    code: 'NS',
                    account: 64,
                    pages: 65,
                    sequence: { start: 1, end: 64 } },
                { name: 'NUSA',
                    code: 'NS',
                    account: 2,
                    pages: 2,
                    sequence: { start: 1, end: 2 } },
                { name: 'PT SAP',
                    code: 'SA',
                    account: 25,
                    pages: 26,
                    sequence: { start: 1, end: 25 } }],
            total: {
                account: 277,
                pages: 281
            }
        },
        {
            prod: 'PLATINUM',
            kur: [
                {
                    name: 'PT POS',
                    code: 'PI',
                    account: 150,
                    pages: 152,
                    sequence: {start: 1, end: 150}
                },
                {
                    name: 'SAP',
                    code: 'SA',
                    account: 36,
                    pages: 36,
                    sequence: {start: 1, end: 36}
                },
                {
                    name: 'NCS',
                    code: 'NS',
                    account: 64,
                    pages: 65,
                    sequence: {start: 1, end: 64}
                },
                {
                    name: 'NUSA',
                    code: 'NS',
                    account: 2,
                    pages: 2,
                    sequence: {start: 1, end: 2}
                },
                {
                    name: 'PT SAP',
                    code: 'SA',
                    account: 25,
                    pages: 26,
                    sequence: {start: 1, end: 25}
                }],
            total: {
                account: 277,
                pages: 281
            }
        }, ]



    const path = `${cfg.assetPath}billnet-files/assets/files/output/samplexxx.xlsx`
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `sample123.xlsx`
    let filepath = cfg.assetPath + 'billnet-files/assets/files/output/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR With Nin';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };
    // add a table to a sheet
    // STYLE
    const totalFill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'00FFFF'}
    };
    const headerFill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'C0C0C0'}
    };
    const headerName = ['Kurir', 'Kode Kurir', 'Account', 'Pages', 'Sequence No', 'Sequence No']
    let cntRow = 4
    data.forEach((el, idx) => {
        worksheet.getCell(cntRow, 1).value = el.prod;
        worksheet.getCell(cntRow, 1).style = { font: { name: 'Arial', size: 15 } }
        cntRow++;
        worksheet.getRow(cntRow).values = ['kurir', 'code', 'acc', 'pages', 'seq_s'];
        worksheet.mergeCells(`E${cntRow}:F${cntRow}`)
        const row = worksheet.getRow(cntRow);
        row.eachCell({ includeEmpty: true }, function(cell, colNumber) {
            // console.log('Cell ' + colNumber + ' = ' + cell.value);
            // console.log(data[idx].kur.length)
            cell.value = headerName[colNumber-1];
            cell.style = { font: { bold: true, name: 'Arial' }};
            cell.alignment = { vertical: 'top', horizontal: 'center' };
            // cell.style = { font: { name: 'Arial', size: 15 } }
            cell.fill = headerFill;
            cell.border = {
                //     // top: {style:'thin'},
                //     // left: {style:'thin'},
                bottom: {style:'medium'},
                //     // right: {style:'thin'}
            }
        });
        worksheet.columns = [
            {key: 'kurir', width: 8},
            {key: 'code', width: 15},
            {key: 'acc', width: 8},
            {key: 'pages', width: 8},
            {key: 'seq_s', width: 8},
            {key: 'seq_e', width: 8},
        ];
        el.kur.forEach(el2 => {
            worksheet.addRow(
                {
                    kurir: el2.name,
                    code: el2.code,
                    acc: el2.account,
                    pages: el2.pages,
                    seq_s: el2.sequence.start,
                    seq_e: el2.sequence.end,
                });
        })
        cntRow += el.kur.length + 1;
        // BOTTOM BORDER
        const row2 = worksheet.getRow(cntRow-1);
        row2.eachCell({ includeEmpty: true }, function(cell, colNumber) {
            console.log('Cell ' + colNumber + ' = ' + cell.value);
            cell.border = {
                //     // top: {style:'thin'},
                //     // left: {style:'thin'},
                bottom: {style:'medium'},
                //     // right: {style:'thin'}
            }
        });
        // TEXT
        worksheet.getCell(cntRow, 1).value = 'Total';
        worksheet.getCell(cntRow, 3).value = el.total.account;
        worksheet.getCell(cntRow, 4).value = el.total.pages;
        const row3 =  worksheet.getRow(cntRow);
        row3.eachCell({ includeEmpty: true }, function(cell, colNumber) {
            cell.fill = totalFill
            cell.style = { font: { bold: true, name: 'Arial' }};
        });

        // FILL
        worksheet.getCell(cntRow, 1).fill = totalFill;
        worksheet.getCell(cntRow, 2).fill = totalFill;
        worksheet.getCell(cntRow, 3).fill = totalFill;
        worksheet.getCell(cntRow, 4).fill = totalFill;
        cntRow += 2;
    })

    await workbook.xlsx.writeFile(filepath + filename)
    res.json({
        success: true,
        message: "write excel ok",
    });
});

// EXCEL TO JSON
router.get('/write-excel-with-table', async (req, res) => {

    const data = [{
        prod: 'BARCA',
        kur: [
            {
                name: 'PT POS',
                code: 'PI',
                account: 150,
                pages: 152,
                sequence: {start: 1, end: 150}
            },
            {
                name: 'SAP',
                code: 'SA',
                account: 36,
                pages: 36,
                sequence: {start: 1, end: 36}
            },
            {
                name: 'NCS',
                code: 'NS',
                account: 64,
                pages: 65,
                sequence: {start: 1, end: 64}
            },
            {
                name: 'NUSA',
                code: 'NS',
                account: 2,
                pages: 2,
                sequence: {start: 1, end: 2}
            },
            {
                name: 'PT SAP',
                code: 'SA',
                account: 25,
                pages: 26,
                sequence: {start: 1, end: 25}
            }]
    },
        {
            prod: 'PLATINUM',
            kur: [
                {
                    name: 'PT POS',
                    code: 'PI',
                    account: 150,
                    pages: 152,
                    sequence: {start: 1, end: 150}
                },
                {
                    name: 'SAP',
                    code: 'SA',
                    account: 36,
                    pages: 36,
                    sequence: {start: 1, end: 36}
                },
                {
                    name: 'NCS',
                    code: 'NS',
                    account: 64,
                    pages: 65,
                    sequence: {start: 1, end: 64}
                },
                {
                    name: 'NUSA',
                    code: 'NS',
                    account: 2,
                    pages: 2,
                    sequence: {start: 1, end: 2}
                },
                {
                    name: 'PT SAP',
                    code: 'SA',
                    account: 25,
                    pages: 26,
                    sequence: {start: 1, end: 25}
                }]
        }, ]



    const path = `${cfg.assetPath}billnet-files/assets/files/output/samplexxx.xlsx`
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `sample123.xlsx`
    let filepath = cfg.assetPath + 'billnet-files/assets/files/output/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR With Nin';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };
    // add a table to a sheet

    let cntRow = 4;
    data.forEach((el, idx) => {
        const r = [];
        el.kur.forEach(el2 => {
            const rx = [el2.name, el2.code, el2.account, el2.pages, el2.sequence.start, el2.sequence.end]
            r.push(rx)
        })
        worksheet.getCell(cntRow, 1).value = el.prod;
        cntRow++;
        worksheet.addTable({
            name: `MyTable_${idx}`,
            ref: `A${cntRow}`,
            headerRow: true,
            totalsRow: true,
            // style: {
            //     theme: 'TableStyleDark3',
            //     showRowStripes: true,
            // },
            columns: [
                {name: 'Kurir', totalsRowLabel: 'Totals:', filterButton: false},
                {name: 'Kode Kurir'},
                {name: 'Account', totalsRowFunction: 'sum', filterButton: false},
                {name: 'Pages', totalsRowFunction: 'sum', filterButton: false},
                {name: 'No Seq'},
                {name: 'No Seq End'},
            ],
            rows: r
        });
        // worksheet.mergeCells('E5:F5');
        // worksheet.mergeCells('E14:F14');
        cntRow += el.kur.length + 3
    })

    // worksheet.columns = [
    //     {key: 'kurir', width: 8},
    //     {key: 'code', width: 8},
    //     {key: 'acc', width: 8},
    //     {key: 'pages', width: 8},
    //     {key: 'seq_s', width: 8},
    //     {key: 'seq_e', width: 8},
    // ];

    // data.forEach((el, i) => {
    //     worksheet.addRow(
    //         {
    //             kurir: el.name,
    //             code: el.code,
    //             acc: el.account,
    //             pages: el.pages,
    //             seq_s: el.sequence.start,
    //             seq_e: el.sequence.end,
    //         });
    //     // console.log(element.merchant)
    //     // console.log(i)
    // });
    // worksheet.eachRow(function(row, rowNumber) {
    //     console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values));
    // });
    // const row = worksheet.lastRow;
    // row.getCell(1).value = 'JUMLAH';
    // row.getCell(1).fill = {
    //     type: 'solid',
    //     pattern:'darkVertical',
    //     // fgColor:{argb:'FFFF0000'}
    //     bgColor:{argb:'FF0000FF'}
    // };

    await workbook.xlsx.writeFile(filepath + filename)
    res.json({
        success: true,
        message: "write excel ok",
    });
});

// EXCEL TO JSON
router.get('/init', async (req, res) => {

    // const testFolder = '/mnt/cdn/bank_mega/data_master_extract/data billing cyc 190121 barca/';
    const testFolder = '/mnt/cdn/bank_mega/data_master_extract/';
    const data = {};

    fs.readdir(testFolder,  async (err, files) => {
        // const parseFiles = files.filter(el => ['.xls', '.xlsx'].includes(path.extname(el).toLowerCase()))
        // const stmtFiles = files.filter(el => !['.xls', '.xlsx', '.txt'].includes(path.extname(el).toLowerCase()))

        // console.log(parseFiles.length);
        console.log(files.filter(x => x.includes('data billing')));

        // const totalFiles = parseFiles.length;
        // let cntFiles = 0

        // for (const file of parseFiles) {
        //
        // }
        res.json({
            success: true,
            message: "EXCEL TO JSON ok",
            // length: data.length,
            data: data,
            // d: master_prod
        });
    });

    // fs.readdir(testFolder,  async (err, files) => {
    //     const parseFiles = files.filter(el => ['.xls', '.xlsx'].includes(path.extname(el).toLowerCase()))
    //     const stmtFiles = files.filter(el => !['.xls', '.xlsx', '.txt'].includes(path.extname(el).toLowerCase()))
    //
    //     // console.log(parseFiles.length);
    //     console.log(stmtFiles);
    //
    //     // const totalFiles = parseFiles.length;
    //     // let cntFiles = 0
    //
    //     for (const file of parseFiles) {
    //         if (file.includes('jangan dicetak')){
    //             data.jangan_dicetak = await init.jangan_di_cetak(testFolder+file)
    //         }else if (file.includes('kuncian kurir')){
    //             data.kuncian_kurir = await init.kuncian_kurir(testFolder+file)
    //         }else if (file.includes('lempar')){
    //             data.lempar = await init.lempar(testFolder+file)
    //         }else if (file.includes('pindah kurir')){
    //             data.pindah_kurir = await init.pindah_kurir(testFolder+file)
    //         }else{
    //             console.log(file)
    //         }
    //         // console.log(file)
    //     }
    //     res.json({
    //         success: true,
    //         message: "EXCEL TO JSON ok",
    //         // length: data.length,
    //         data: data,
    //         // d: master_prod
    //     });
    // });
});

// UNZIP ALL FILE #STEP 1
router.get('/unzip-file-1', async (req, res) => {

    const testFolder = '/mnt/cdn/bank_mega/data_master/';
    const fs = require('fs');


    fs.readdir(testFolder,  (err, files) => {
        const parseFiles = files.filter(el => path.extname(el) === '.zip')
        // console.log(parseFiles.length);
        const totalFiles = parseFiles.length;
        let cntFiles = 0
        parseFiles.forEach(file => {
            if (file.includes('.zip')){
                const zipPath = `/mnt/cdn/bank_mega/data_master/${file}`
                const zipExtPath = zipPath.replace('.zip', '/').replace('data_master', 'data_master_extract')
                console.log(`start unziping ${zipPath} at ${moment().format('HH:mm:ss')}`)
                unzipper.Open.file(zipPath)
                    .then(d => d.extract({path: zipExtPath, concurrency: 5, password: 'visa'})
                    )
                    .finally(() => {
                        cntFiles++
                        console.log(`${cntFiles} / ${totalFiles}`)
                        console.log(`extracting zip ${zipPath} at ${moment().format('HH:mm:ss')}`)
                        if (cntFiles === totalFiles){
                            console.log('unzipped all files done.')
                        }
                    });
            }
        });
    });

    // const zipPath = '/mnt/cdn/bank_mega/data_master/data billing cyc 190121 barca.zip'
    // const directory = await unzipper.Open.file(zipPath);
    // const extracted = await directory.files[0].buffer('visa');
    // //
    // console.log(extracted)
    // const zipExtPath = zipPath.replace('.zip', '/')
    // const zip = new AdmZip(zipPath);
    // const zipEntries = zip.getEntries(); // an array of ZipEntry records
    // zipEntries.forEach(function(zipEntry) {
    //     // console.log(zipEntry.toString()); // outputs zip entries information
    //     // if (zipEntry.entryName === "my_file.txt") {
    //     //     console.log(zipEntry.getData().toString('utf8'));
    //     // }
    // });
    // // outputs the content of some_folder/my_file.txt
    // // console.log(zip.readAsText("some_folder/my_file.txt"));
    // // extracts the specified file to the specified location
    // // zip.extractEntryTo(/*entry name*/"some_folder/my_file.txt", /*target path*/"/home/me/tempfolder", /*maintainEntryPath*/false, /*overwrite*/true);
    // // extracts everything
    // // zip.extractAllTo(/*target path*/zipExtPath, /*overwrite*/true, );
    // // console.log(`start unziping ${zipPath} at ${moment().format('HH:mm:ss')}`)
    // // unzipper.Open.file(zipPath)
    // //     .then(d => d.extract({path: zipExtPath, concurrency: 5, password: 'visa'})
    // //     )
    // //     .finally(() => {
    // //         console.log(`extracting zip ${zipPath} done at ${moment().format('HH:mm:ss')}`)
    // //     });

    res.json({
        success: true,
        message: "read dir ok",
        // length: data.length,
        // data: data
    });
});

// UNZIP ALL FILE #STEP 1
router.get('/zipfile', async (req, res) => {

    const cycle = '20210524';
    // create a file to stream archive data to.
    const output = fs.createWriteStream(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/SAMPLE_APP_MEGA_${cycle}.zip`);
    const archive = archiver('zip', {
        zlib: { level: 9 }, // Sets the compression level.
    });

// listen for all archive data to be written
// 'close' event is fired only when a file descriptor is involved
    output.on('close', function() {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
    output.on('end', function() {
        console.log('Data has been drained');
    });

// good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function(err) {
        if (err.code === 'ENOENT') {
            // log warning
        } else {
            // throw error
            throw err;
        }
    });

// good practice to catch this error explicitly
    archive.on('error', function(err) {
        throw err;
    });

// pipe archive data to the file
    archive.pipe(output);

// // append a file from stream
//     const file1 = __dirname + '/file1.txt';
//     archive.append(fs.createReadStream(file1), { name: 'file1.txt' });
//
// // append a file from string
//     archive.append('string cheese!', { name: 'file2.txt' });
//
// // append a file from buffer
//     const buffer3 = Buffer.from('buff it!');
//     archive.append(buffer3, { name: 'file3.txt' });
//
// append a file
    archive.file(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/summary_stfile.TXT`, { name: 'summary_stfile.TXT' });
    archive.file(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/Rekapitulasi_Bankmega_Cyc${cycle}_Produksi.xlsx`, { name: `Rekapitulasi_Bankmega_Cyc${cycle}_Produksi.xlsx` });
    archive.file(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/Rekapitulasi_Bankmega_Cyc${cycle}.xlsx`, { name: `Rekapitulasi_Bankmega_Cyc${cycle}.xlsx` });
//
// // append files from a sub-directory and naming it `new-subdir` within the archive
    archive.directory(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/LISTING_KOTA`, 'LISTING_KOTA');
    archive.directory(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/LOG_NASABAH_CETAK`, 'LOG_NASABAH_CETAK');
    archive.directory(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/LOG_NASABAH_HOLD_(TIDAK_CETAK)`, 'LOG_NASABAH_HOLD_(TIDAK_CETAK)');
    archive.directory(`/mnt/cdn/billnet-files/assets/files/output/${cycle}/SAMPLE_PDF`, 'SAMPLE_PDF');

// append files from a sub-directory, putting its contents at the root of archive
//     archive.directory('/mnt/cdn/billnet-files/assets/files/output/20210516/LISTING_KOTA', false);
//     archive.directory('/mnt/cdn/billnet-files/assets/files/output/20210516/LOG_NASABAH_CETAK', false);
//     archive.directory('/mnt/cdn/billnet-files/assets/files/output/20210516/LOG_NASABAH_HOLD_(TIDAK_CETAK)', false);
//     archive.directory('/mnt/cdn/billnet-files/assets/files/output/20210516/SAMPLE_PDF', false);

// // append files from a glob pattern
//     archive.glob('file*.txt', {cwd:__dirname});

// finalize the archive (ie we are done appending files but streams have to finish yet)
// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
    archive.finalize();

    res.json({
        success: true,
        message: "zip ok",
        // length: data.length,
        // data: data
    });
});

//  #STEP 2
router.get('/dir-reader-2', async (req, res) => {

    const testFolder = '/mnt/cdn/bank_mega/data_master_extract/';

    fs.readdir(testFolder,  (err, files) => {
        const parseFiles = files.filter(el => fs.lstatSync(testFolder+el).isDirectory())
        // console.log(parseFiles.length);

        // const totalFiles = parseFiles.length;
        // let cntFiles = 0
        parseFiles.forEach(file => {
            console.log(file)
        });
    });

    res.json({
        success: true,
        message: "read dir ok",
        // length: data.length,
        // data: data
    });
});

// UPDATE
router.post('/resend-email/update-email', (req, res) =>  {
    // db.select(
    //     'id',
    //     'name'
    // )
    //     .from('department')
    //     .where({
    //         name: req.body.name,
    //     })
    //     .andWhere('id', '<>', req.body.id)
    //     .then(data2 => {
    //         if (data2.length === 0){
                db('tb_email_1810')
                    .where('id', req.body.id)
                    .update({email: req.body.email})
                    .then(data => {
                        res.json({
                            success: true,
                            message: "Update email success.",
                            count: data.length,
                            data: data,
                        });

                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({
                            success: false,
                            message: 'update email failed',
                            data: err
                        })
                    })
        //     }else{
        //         res.json({
        //             success: false,
        //             message: 'email already exist',
        //             // count: data,
        //             // data: result,
        //         });
        //     }
        //     return null
        // })
        // .catch((err) =>{
        //     console.log(err)
        //     res.json({
        //         success: false,
        //         message: "Update email failed.",
        //         // count: data.length,
        //         data: err,
        //     });
        // });
});

// DELETE
router.delete('/delete/:id', (req, res) => {

    db('department').where({ id: req.params.id }).del().then((data) => {
        res.json({
            success: true,
            message: "Delete department success",
            data: data,
        });
    });
});

module.exports = router;

// INIT PRIMARY PDF FILE AND PATH
function initPDFDoc(fh, index_stmt) {
    const pdfDoc = new PDFDocument({
        size: 'A4',
        bufferPages: true
    });

    layout1.initFont(pdfDoc);
    let sPath = `${outputPath+fh.periode}/PDF/${fh.detail.name}/`;
    // let filename = random+'.pdf'
    if (!fs.existsSync(sPath)) {
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(sPath);
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    // doc.doc.pipe(fs.createWriteStream(`${sPath}NO_KURIR_${detail.jenis}${(index+1)}_000.pdf`));
    //
    pdfDoc.pipe(fs.createWriteStream(`${sPath + fh.kurir.name}_${fh.detail.jenis}${(index_stmt)}_000.pdf`));
    return pdfDoc;
};

// SET PRIMARY LAYOUT
function setLayout(doc, data, isSample){
    doc.addPage()
    layout1.header(doc, data, isSample)
    layout1.subHeader(doc, data)
    layout1.body(doc, data)
    switch (data.fh.detail.name){
        case 'cashline':
            layout1.footer_cashline(doc, data)
            break;
        case 'travel':
            layout1.footer(doc, data)
            break
        default:
            layout1.footer(doc, data)
            break;
    }

}

// SORTED KURIR
function sortKurir(data, dataInit) {
    // console.log(dataInit.kuncian_kurir.filter(x => x.NO_KARTU === data.ah.card_no))
    const kuncianKurir = dataInit.kuncian_kurir.filter(x => x.NO_KARTU === data.ah.card_no) // PARSE KUNCIAN KURIR
    if (kuncianKurir.length !== 0){
        const kr = kuncianKurir[0].DIALIHKAN;
        data.fh.kurir = {
            fullname : master_kurir.filter(x => x.fullname === kr)[0].fullname,
            name: master_kurir.filter(x => x.fullname === kr)[0].code,
            nfile: kuncianKurir[0].KOTA,
            aliases: master_kurir.filter(x => x.fullname === kr)[0].aliases
        }
    }else{
        // const d = dataInit.sortasi_kurir.filter(x => stringSimilarity.compareTwoStrings(x.ADDR1, data.ah.addr5) >= 0.9 || stringSimilarity.compareTwoStrings(x.ADDR1, data.ah.addr3) >= 0.9 || stringSimilarity.compareTwoStrings(x.ADDR1, data.ah.addr5) >= 0.9)
        let d = dataInit.sortasi_kurir.filter(x => stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr5.toUpperCase()) >= 0.9 || stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr4.toUpperCase()) >= 0.9)
        // PARSING USING SIMILARITY TECHNIC
        d.length === 0 ? d = dataInit.sortasi_kurir.filter(x => stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr5.toUpperCase()) >= 0.8 || stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr4.toUpperCase()) >= 0.8) :
            d.length === 0 ? d = dataInit.sortasi_kurir.filter(x => stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr5.toUpperCase()) >= 0.7 || stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr4.toUpperCase()) >= 0.7) :
                d.length === 0 ? d = dataInit.sortasi_kurir.filter(x => stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr5.toUpperCase()) >= 0.6 || stringSimilarity.compareTwoStrings(x.ADDR1.toUpperCase(), data.ah.addr4.toUpperCase()) >= 0.6) :
                    null

        // if (d.length === 0){
        //     console.log(d)
        //     console.log(data.ah.addr4+',', data.ah.addr5)
        // }
        const kr = d.length !== 0 ? d[0].KURIR : '';
        if (data.ah.addr1.includes('BANK MEGA') || data.ah.addr1.includes('TRANS TV')){
            data.fh.kurir = {
                fullname : 'CARD CENTER',
                name: 'CC',
                nfile: data.ah.addr5, //d[0].ADDR2,
                aliases: 'CARD_CENTER'
            }
        }else if (data.ah.addr1.length < 3 && data.ah.addr2.length < 3 && data.ah.addr3.length < 3 && data.ah.addr4.length < 3 && data.ah.addr5.length < 3 && data.ah.company_name.length < 3){
            data.fh.kurir = {
                fullname : 'CARD CENTER',
                name: 'CC',
                nfile: 'NO ADDRESS', //d[0].ADDR2,
                aliases: 'CARD_CENTER'
            }
        }
        else{
            d.length !== 0 ?
                data.fh.kurir = {
                    fullname : master_kurir.filter(x => x.fullname === kr)[0].fullname,
                    name: master_kurir.filter(x => x.fullname === kr)[0].code,
                    nfile: data.ah.addr5, //d[0].ADDR2,
                    aliases: master_kurir.filter(x => x.fullname === kr)[0].aliases
                } : data.fh.kurir = {
                    // IF ADDRESS IS NOT IN MASTER KURIR
                    fullname: 'POS',
                    nfile: 'NO ADDRESS NOT FOUND ON SORTIR',
                    name: 'PI',
                    aliases : 'POS'
                    // fullname: 'CARD CENTER',
                    // nfile: 'NO ADDRESS NOT FOUND ON SORTIR',
                    // name: 'CC',
                    // aliases : 'CARD_CENTER'
                };
        }
    }
}

// PARSING FILE REQUEST LEMPAR
function requestLempar(data, dataInit) {
    const data_lempar = dataInit.lempar.filter(x => x.NO_KARTU === data.ah.card_no)
    data_lempar.length !== 0 ? data.ah.request = data_lempar[0].REQUEST : data.ah.request = ''
}

// INIT SAMPLE PDF FILE AND PATH
function initSamplePDF (fh) {
    const pdfDoc = new PDFDocument({
        size: 'A4',
        bufferPages: true
    });
    layout1.initFont(pdfDoc)
    let sPath = outputPath + fh.periode + '/SAMPLE_PDF/';
    // let filename = random+'.pdf'
    if (!fs.existsSync(sPath)) {
        console.log('file not exist and will be created...');
        const made = mkdirp.sync(sPath)
        console.log(`made directories, starting with ${made}`);
        console.log('done create directory')
    }

    fs.writeFileSync(`${sPath}NOTE.txt`,
        'Kode B = Barcelona/Barca\n' +
        'Kode C = Carefour\n' +
        'Kode P = Platinum\n' +
        'Kode R = Reguler\n' +
        'Kode M = Transmart\n' +
        'Kode T = Travel\n' +
        'Kode S = Sulteng\n' +
        'Kode L = Cashline\n' +
        'Kode A = Affinity\n' +
        'kode W = Wholeshale');
    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    // doc.doc.pipe(fs.createWriteStream(`${sPath}NO_KURIR_${detail.jenis}${(index+1)}_000.pdf`));
    //
    pdfDoc.pipe(fs.createWriteStream(`${sPath}Sample_${fh.detail.jenis}.pdf`));
    return pdfDoc;
}

// CREATE SAMPLE PDF
function createSample (doc, data) {
    const cntSample = Object.values(data.fh.seq_user_per_pdf).reduce(function (acc, arr) { return acc + arr; }, 0);
    if (cntSample < 10){
        setLayout(doc, data, true)
    }else if (cntSample === 11){
        doc.switchToPage(0); // switch page to 1
        layout1.coverSample(doc, data); // set cover on page 1
        doc.end();
    }
}

function summary_prod(data){
    let result = {
        ttl: 0,
        code: data.filename
    }
    switch (data.filename){
        // POS
        case 'PI_P':
            result.prod = 'platinum';
            result.ttl = data.ttl;
            break;
        case 'PI_B':
            result.prod = 'barca'
            result.ttl = data.ttl;
            break;
        case 'PI_C':
            result.prod = 'carrefour'
            result.ttl = data.ttl;
            break;
        case 'PI_W':
            result.prod = 'wholesale'
            result.ttl = data.ttl;
            break;
        case 'PI_T':
            result.prod = 'travel'
            result.ttl = data.ttl;
            break;
        case 'PI_A':
            result.prod = 'anfinity'
            result.ttl = data.ttl;
            break;
        case 'PI_L':
            result.prod = 'cashline'
            result.ttl = data.ttl;
            break;
        case 'PI_M':
            result.prod = 'transmart'
            result.ttl = data.ttl;
            break;
        case 'PI_S':
            result.prod = 'sulteng'
            result.ttl += data.ttl;
            break;
        case 'PI_R':
            result.prod = 'reguler'
            result.ttl = data.ttl;
            break;
        // NCS
        case 'NS_P':
            result.prod = 'platinum'
            result.ttl = data.ttl;
            break;
        case 'NS_B':
            result.prod = 'barca'
            result.ttl = data.ttl;
            break;
        case 'NS_C':
            result.prod = 'carrefour'
            result.ttl = data.ttl;
            break;
        case 'NS_W':
            result.prod = 'wholesale'
            result.ttl = data.ttl;
            break;
        case 'NS_T':
            result.prod = 'travel'
            result.ttl = data.ttl;
            break;
        case 'NS_A':
            result.prod = 'anfinity'
            result.ttl = data.ttl;
            break;
        case 'NS_L':
            result.prod = 'cashline'
            result.ttl = data.ttl;
            break;
        case 'NS_M':
            result.prod = 'transmart'
            result.ttl = data.ttl;
            break;
        case 'NS_S':
            result.prod = 'sulteng'
            result.ttl = data.ttl;
            break;
        case 'NS_R':
            result.prod = 'reguler'
            result.ttl = data.ttl;
            break;
        // SAP
        case 'SA_P':
            result.prod = 'platinum'
            result.ttl = data.ttl;
            break;
        case 'SA_B':
            result.prod = 'barca'
            result.ttl = data.ttl;
            break;
        case 'SA_C':
            result.prod = 'carrefour'
            result.ttl = data.ttl;
            break;
        case 'SA_W':
            result.prod = 'wholesale'
            result.ttl = data.ttl;
            break;
        case 'SA_T':
            result.prod = 'travel'
            result.ttl = data.ttl;
            break;
        case 'SA_A':
            result.prod = 'anfinity'
            result.ttl = data.ttl;
            break;
        case 'SA_L':
            result.prod = 'cashline'
            result.ttl = data.ttl;
            break;
        case 'SA_M':
            result.prod = 'transmart'
            result.ttl = data.ttl;
            break;
        case 'SA_S':
            result.prod = 'sulteng'
            result.ttl = data.ttl;
            break;
        case 'SA_R':
            result.prod = 'reguler'
            result.ttl = data.ttl;
            break;
    }
    return result;
}
