const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
moment.locale('id')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const cfg = require('../../../../config');
const fs = require('fs');
const { promisify } = require('util');
const readdir = promisify(require('fs').readdir);
const PDFMerger = require('pdf-merger-js');
import { degrees, PDFDocument, rgb, StandardFonts } from 'pdf-lib';
const mkdirp = require('mkdirp');
const pdf = require('pdf-page-counter');
const unzipper = require('unzipper');
const Minizip = require('minizip-asm.js');
const mz = new Minizip();
const excelRekapMerger = require('../../../../plugins/excel');
// pdf.createPDF()

const outputPath = cfg.assetPath + 'billnet-files/assets/files/output/';
const inputPath = cfg.assetPath + 'bank_mega/data_master_extract/';


router.get('/', (req, res) => {
    res.send('We Are In GENERATE MERGER Route')
    console.log(moment('2021-05-21').diff(moment('2021-01-21'), 'months', true))

})

// GENERATE MEGAJIWA MANUAL
router.get('/posmegajiwa1/date=:date/dir=:dir', async (req, res) => {
    const path_folder = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/`;
    const path_result = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/merger/`;
    if (!fs.existsSync(path_result)) {
        console.log('dir not exist and will be created...');
        const made = mkdirp.sync(path_result);
        console.log(`made directories, starting with ${made}`);
        console.log('done create directory');
    }
    const files = await readdir(path_folder); // single process
    const merger_hal_1 = new PDFMerger();
    const merger_hal_2_up = new PDFMerger();
    const merger_hal1_hal2_up = new PDFMerger();
    (async () => {
        let cnt = 0
        let arr_num = []
        // console.log(files)
        for await (const el of files){
            if (el.includes('.pdf')){
                // console.log(el)
                cnt++
                let dataBuffer = await fs.readFileSync(path_folder+el);
                const pdfPages = await pdf(dataBuffer).then(function(data) {
                    return data.numpages
                });
                // console.log(pdfPages)
                arr_num.push({hal: pdfPages, num: null})
                if (pdfPages > 1){
                    merger_hal_2_up.add(path_folder+el); // merge only page 2
                }else{
                    // for (let x = 0; x < pdfPages; x++){
                    //     arr_num.push({hal: pdfPages, num: cnt})
                    // }
                    merger_hal_1.add(path_folder+el); // merge only page 2
                }
            }
        }
        arr_num.sort((a,b) => (a.hal > b.hal) ? 1 : ((b.hal > a.hal) ? -1 : 0))
        console.log(arr_num)
        console.log(cnt)
        await merger_hal_1.save(`${path_result}merger-hal-1.pdf`); //save under given name and reset the internal document
        await merger_hal_2_up.save(`${path_result}merger-hal-2-up.pdf`); //save under given name and reset the internal document
        //
        merger_hal1_hal2_up.add(`${path_result}merger-hal-1.pdf`); // merge only page 2
        merger_hal1_hal2_up.add(`${path_result}merger-hal-2-up.pdf`); // merge only page 2
        await merger_hal1_hal2_up.save(`${path_result}merged-fix.pdf`); //save under given name and reset the internal document
        //
        // const data = fs.readFileSync(`${path_result}merged-without-number.pdf`)
        const data = fs.readFileSync(`${path_result}merged-fix.pdf`)
        // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
        // let bytes = new Uint8Array(existingPdfBytes);
        const pdfDoc = await PDFDocument.load(data)
        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
        // Get the first page of the document
        const pages = pdfDoc.getPages()
        // const firstPage = pages[0]

        // Get the width and height of the first page
//         const { width, height } = firstPage.getSize()

// Draw a string of text diagonally across the first page
        for await (const [i, el] of pages.entries()) {
            el.drawText((i+1).toString().padStart(4, "0"), {
                x: 45,
                // y: height / 2,
                y: 610,
                size: 7,
                font: helveticaFont,
                // color: rgb(0.95, 0.1, 0.1),
                rotate: degrees(90),
            })
        }
// Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save()
        fs.writeFileSync(`${path_result}` + req.params.dir + req.params.date + '.pdf', pdfBytes)
        fs.unlinkSync(`${path_result}merger-hal-1.pdf`)
        fs.unlinkSync(`${path_result}merger-hal-2-up.pdf`)
        fs.unlinkSync(`${path_result}merged-fix.pdf`)
        // console.log(pdfBytes)
    })();
    res.json({
        message: 'oke pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});
// GENERATE MEGAJIWA SAMPLE
router.get('/posmegajiwa2/date=:date', async (req, res) => {

    const path_folder = `/mnt/cdn/mega-jiwa-pos/202107/${req.params.date}/`;
    const path_result = `/mnt/cdn/mega-jiwa-pos/202107/${req.params.date}/merger/`;
    if (!fs.existsSync(path_result)) {
        console.log('dir not exist and will be created...');
        const made = mkdirp.sync(path_result);
        console.log(`made directories, starting with ${made}`);
        console.log('done create directory');
    }
    const files = await readdir(path_folder); // single process
    const merger = new PDFMerger();
    const merger2 = new PDFMerger();
    const merger3 = new PDFMerger();
    (async () => {
        let cnt = 0
        // console.log(files)
        for await (const el of files){
            if (el.includes('.pdf')){
                console.log(el)
                cnt++
                let dataBuffer = await fs.readFileSync(path_folder+el);
                const pdfPages = await pdf(dataBuffer).then(function(data) {

                    return data.numpages
                    // number of pages
                    // console.log(data.numpages);
                    // // number of rendered pages
                    // console.log(data.numrender);
                    // // PDF info
                    // console.log(data.info);
                    // // PDF metadata
                    // console.log(data.metadata);
                    // // PDF.js version
                    // // check https://mozilla.github.io/pdf.js/getting_started/
                    // console.log(data.version);
                    // // PDF text
                    // console.log(data.text);

                });
                // console.log(pdfPages)
                merger.add(path_folder+el, [1]); // merge only page 2
                if (pdfPages > 1){
                    merger2.add(path_folder+el, '2-'+pdfPages); // merge only page 2
                }
            }
        }
        // files.forEach((el) => {
        //     if (el.includes('.pdf')){
        //         cnt++
        //         let dataBuffer = fs.readFileSync(path_folder+el);
        //         const pdfPages = pdf(dataBuffer).then(function(data) {
        //
        //             return data.numpages
        //             // number of pages
        //             // console.log(data.numpages);
        //             // // number of rendered pages
        //             // console.log(data.numrender);
        //             // // PDF info
        //             // console.log(data.info);
        //             // // PDF metadata
        //             // console.log(data.metadata);
        //             // // PDF.js version
        //             // // check https://mozilla.github.io/pdf.js/getting_started/
        //             // console.log(data.version);
        //             // // PDF text
        //             // console.log(data.text);
        //
        //         });
        //         console.log(pdfPages)
        //         merger.add(path_folder+el, [1]); // merge only page 2
        //         // merger2.add(path_folder+el, '2-'+); // merge only page 2
        //     }
        // })
        // merger.add('pdf1.pdf');  //merge all pages. parameter is the path to file and filename.
        // merger.add('pdf2.pdf', [1]); // merge only page 2
        // merger.add('pdf2.pdf', [1, 3]); // merge the pages 1 and 3
        // merger.add('pdf2.pdf', '4, 7, 8'); // merge the pages 4, 7 and 8
        // merger.add('pdf3.pdf', '1 to 2'); //merge pages 1 to 2
        // merger.add('pdf3.pdf', '3-4'); //merge pages 3 to 4
        await merger.save(`${path_result}merged-without-number.pdf`); //save under given name and reset the internal document
        await merger2.save(`${path_result}merged-extended.pdf`); //save under given name and reset the internal document
        //
        merger3.add(`${path_result}merged-without-number.pdf`); // merge only page 2
        merger3.add(`${path_result}merged-extended.pdf`); // merge only page 2
        await merger3.save(`${path_result}merged-fix.pdf`); //save under given name and reset the internal document
        //
        // const data = fs.readFileSync(`${path_result}merged-without-number.pdf`)
        const data = fs.readFileSync(`${path_result}merged-fix.pdf`)
        // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
        // let bytes = new Uint8Array(existingPdfBytes);
        const pdfDoc = await PDFDocument.load(data)
        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
        // Get the first page of the document
        const pages = pdfDoc.getPages()
        // const firstPage = pages[0]

        // Get the width and height of the first page
//         const { width, height } = firstPage.getSize()

// Draw a string of text diagonally across the first page
        for await (const [i, el] of pages.entries()) {
            el.drawText((i+1).toString().padStart(4, "0"), {
                x: 45,
                // y: height / 2,
                y: 610,
                size: 7,
                font: helveticaFont,
                // color: rgb(0.95, 0.1, 0.1),
                rotate: degrees(90),
            })
        }
//         pages.forEach((el, index) => {
//             el.drawText((index+1).toString().padStart(4, "0"), {
//                 x: 45,
//                 // y: height / 2,
//                 y: 610,
//                 size: 8,
//                 font: helveticaFont,
//                 // color: rgb(0.95, 0.1, 0.1),
//                 rotate: degrees(90),
//             })
//             console.log(index+1)
//         })
        // firstPage.drawText('0001', {
        //     x: 45,
        //     // y: height / 2,
        //     y: 607,
        //     size: 15,
        //     font: helveticaFont,
        //     // color: rgb(0.95, 0.1, 0.1),
        //     rotate: degrees(90),
        // })


// Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save()
        fs.writeFileSync(`${path_result}result-merge.pdf`, pdfBytes)
        // console.log(pdfBytes)
    })();
    res.json({
        message: 'oke pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});
// GENERATE MEGAJIWA AUTO
router.get('/posmegajiwa3/date=:date/dir=:dir', async (req, res) => {
    const path_folder = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/`;
    const path_result = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/merger/`;
    if (!fs.existsSync(path_result)) {
        console.log('dir not exist and will be created...');
        const made = mkdirp.sync(path_result);
        console.log(`made directories, starting with ${made}`);
        console.log('done create directory');
    }
    const files = await readdir(path_folder); // single process
    const merger_hal_1 = new PDFMerger();
    const merger_hal_2_up = new PDFMerger();
    const merger_hal1_hal2_up = new PDFMerger();
    (async () => {
        let cnt = 0
        // console.log(files)
        for await (const el of files){
            if (el.includes('.pdf')){
                console.log(el)
                cnt++
                let dataBuffer = await fs.readFileSync(path_folder+el);
                const pdfPages = await pdf(dataBuffer).then(function(data) {
                    return data.numpages
                });
                // console.log(pdfPages)

                if (pdfPages > 1){
                    merger_hal_2_up.add(path_folder+el); // merge only page 2
                }else{
                    merger_hal_1.add(path_folder+el); // merge only page 2
                }
            }
        }
        await merger_hal_1.save(`${path_result}merger-hal-1.pdf`); //save under given name and reset the internal document
        await merger_hal_2_up.save(`${path_result}merger-hal-2-up.pdf`); //save under given name and reset the internal document
        //
        merger_hal1_hal2_up.add(`${path_result}merger-hal-1.pdf`); // merge only page 2
        merger_hal1_hal2_up.add(`${path_result}merger-hal-2-up.pdf`); // merge only page 2
        await merger_hal1_hal2_up.save(`${path_result}merged-fix.pdf`); //save under given name and reset the internal document
        //
        // const data = fs.readFileSync(`${path_result}merged-without-number.pdf`)
        const data = fs.readFileSync(`${path_result}merged-fix.pdf`)
        // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
        // let bytes = new Uint8Array(existingPdfBytes);
        const pdfDoc = await PDFDocument.load(data)
        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
        // Get the first page of the document
        const pages = pdfDoc.getPages()
        // const firstPage = pages[0]

        // Get the width and height of the first page
//         const { width, height } = firstPage.getSize()

// Draw a string of text diagonally across the first page
        for await (const [i, el] of pages.entries()) {
            el.drawText((i+1).toString().padStart(4, "0"), {
                x: 45,
                // y: height / 2,
                y: 610,
                size: 7,
                font: helveticaFont,
                // color: rgb(0.95, 0.1, 0.1),
                rotate: degrees(90),
            })
        }
// Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save()
        fs.writeFileSync(`${path_result}result-merge.pdf`, pdfBytes)
        // console.log(pdfBytes)
    })();
    res.json({
        message: 'oke pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});
// GENERATE MEGAJIWA DEVELOPMENT
router.get('/posmegajiwa/date=:date/dir=:dir', async (req, res) => {
    (async () => {
        const path_folder = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/`;
        const path_result = `/mnt/cdn/mega-jiwa-pos/${req.params.dir}/${req.params.date}/merger/`;

        if (!fs.existsSync(path_result)) {
            console.log('dir not exist and will be created...');
            const made = mkdirp.sync(path_result);
            console.log(`made directories, starting with ${made}`);
            console.log('done create directory');
        }
        const files = await readdir(path_folder); // single process
        let cnt = 0
        // let arr_num = []
        const merger = []
        const rekap = {
            filename: req.params.dir + req.params.date,
            amplop: 0,
            kertas: 0
        }
        const merger_fix = new PDFMerger();
        // console.log(files)
        for await (const el of files){
            if (el.includes('.pdf')){
                // console.log(el)
                cnt++
                let dataBuffer = await fs.readFileSync(path_folder+el);
                const pdfPages = await pdf(dataBuffer).then(function(data) {
                    return data.numpages
                });
                rekap.amplop = cnt
                rekap.kertas += pdfPages
                if (!merger.find(x => x.id === pdfPages)){
                    merger.push({
                        id: pdfPages,
                        merger : new PDFMerger()
                    })
                }
                // console.log(merger.find(x => x.id === pdfPages))
                merger.find(x => x.id === pdfPages).merger.add(path_folder+el)
            }
        }
        // arr_num.sort((a,b) => (a.hal > b.hal) ? 1 : ((b.hal > a.hal) ? -1 : 0))
        console.log(rekap)
        for await (const el of merger){
            await el.merger.save(`${path_result}merger-hal-${el.id}.pdf`)
        }
        const files_merger = await readdir(path_result); // single process
        for await (const el of files_merger){
            if (el.includes('merger-hal-')){
                merger_fix.add(path_result + el)
            }
        }
        await merger_fix.save(`${path_result}merged-fix.pdf`)
        const data = fs.readFileSync(`${path_result}merged-fix.pdf`)
        // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
        // let bytes = new Uint8Array(existingPdfBytes);
        const pdfDoc = await PDFDocument.load(data)
        // Embed the Helvetica font
        const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
        // Get the first page of the document
        const pages = pdfDoc.getPages()
        // const firstPage = pages[0]
        // console.log(pages.length)
        // Get the width and height of the first page
//         const { width, height } = firstPage.getSize()

// Draw a string of text diagonally across the first page
        for await (const [i, el] of pages.entries()) {
            el.drawText((i + 1).toString().padStart(4, "0"), {
                x: 45,
                // y: height / 2,
                y: 610,
                size: 7,
                font: helveticaFont,
                // color: rgb(0.95, 0.1, 0.1),
                rotate: degrees(90),
            })
        }
// Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save()
        fs.writeFileSync(`${path_result}` + req.params.dir + req.params.date + '.pdf', pdfBytes)
        // fs.unlinkSync(`${path_result}merger-hal-1.pdf`) // delete files
        // fs.unlinkSync(`${path_result}merger-hal-2-up.pdf`) // delete files
        // fs.unlinkSync(`${path_result}merged-fix.pdf`) // delete files
//         console.log(pdfBytes)
    })();
    res.json({
        message: 'oke pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});

// GENERATE MEGAJIWA ALL
router.get('/posmegajiwa', async (req, res) => {
    (async () => {
        const listRekap = []
        try {
            const path_dir = `/mnt/cdn/mega-jiwa-pos/data/files/`;
            const path_result = `/mnt/cdn/mega-jiwa-pos/data/output/`;

            [path_result, path_dir].forEach(el => {
                if (!fs.existsSync(el)) {
                    console.log('dir not exist and will be created...');
                    const made = mkdirp.sync(el);
                    console.log(`made directories, starting with ${made}`);
                    console.log('done create directory');
                }
            })
            const files = await readdir(path_dir); // single process
            for (const el of files.filter(x => x !== '.DS_Store')) {
                const files_dir = await readdir(path_dir+el);
                for (const el2 of files_dir.filter(x => x !== '.DS_Store')) {
                    // const files_dir = await readdir(path_dir+el);
                    // console.log(el2)
                    const files = await readdir(path_dir + el + '/' + el2); // single process
                    let cnt = 0
                    // let arr_num = []
                    const merger = []
                    const rekap = {
                        filename: el + el2,
                        amplop: 0,
                        kertas: 0
                    }
                    const merger_fix = new PDFMerger();
                    // console.log(files)
                    for await (const file_pdf of files){
                        if (file_pdf.includes('.pdf')){
                            console.log(el, el2, file_pdf)
                            // console.log(el)
                            cnt++
                            let dataBuffer = await fs.readFileSync(path_dir + el + '/' + el2 + '/' + file_pdf);
                            const pdfPages = await pdf(dataBuffer).then(function(data) {
                                return data.numpages
                            });
                            rekap.amplop = cnt
                            rekap.kertas += pdfPages
                            if (!merger.find(x => x.id === pdfPages)){
                                merger.push({
                                    id: pdfPages,
                                    merger : new PDFMerger()
                                })
                            }
                            // console.log(merger.find(x => x.id === pdfPages))
                            merger.find(x => x.id === pdfPages).merger.add(path_dir + el + '/' + el2 + '/' + file_pdf)
                        }
                    }
                    // arr_num.sort((a,b) => (a.hal > b.hal) ? 1 : ((b.hal > a.hal) ? -1 : 0))
                    listRekap.push(rekap)
                    if (!fs.existsSync(path_dir + el + '/' + el2 + '/merger/')) {
                        console.log('dir not exist and will be created...');
                        const made = mkdirp.sync(path_dir + el + '/' + el2 + '/merger/');
                        console.log(`made directories, starting with ${made}`);
                        console.log('done create directory');
                    }
                    for await (const el3 of merger){
                        await el3.merger.save(`${path_dir + el + '/' + el2 + '/merger/'}merger-hal-${el3.id}.pdf`)
                    }
                    const files_merger = await readdir(path_dir + el + '/' + el2 + '/merger/'); // single process
                    for await (const el4 of files_merger){
                        if (el4.includes('merger-hal-')){
                            merger_fix.add( path_dir + el + '/' + el2 + '/merger/' + el4)
                        }
                    }
                    await merger_fix.save(`${path_dir + el + '/' + el2 + '/merger/'}merged-fix.pdf`)
                    const data = fs.readFileSync(`${path_dir + el + '/' + el2 + '/merger/'}merged-fix.pdf`)
                    // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
                    // let bytes = new Uint8Array(existingPdfBytes);
                    const pdfDoc = await PDFDocument.load(data)
                    // Embed the Helvetica font
                    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
                    // Get the first page of the document
                    const pages = pdfDoc.getPages()
                    // const firstPage = pages[0]
                    // console.log(pages.length)
                    // Get the width and height of the first page
                    // const { width, height } = firstPage.getSize()

                    // Draw a string of text diagonally across the first page
                    for await (const [i, el5] of pages.entries()) {
                        el5.drawText((i + 1).toString().padStart(4, "0"), {
                            x: 45,
                            // y: height / 2,
                            y: 610,
                            size: 7,
                            font: helveticaFont,
                            // color: rgb(0.95, 0.1, 0.1),
                            rotate: degrees(90),
                        })
                    }
                    // Serialize the PDFDocument to bytes (a Uint8Array)
                    const pdfBytes = await pdfDoc.save()
                    fs.writeFileSync(`${path_result}` + el + el2 + '.pdf', pdfBytes)
                    // fs.unlinkSync(`${path_result}merger-hal-1.pdf`) // delete files
                    // fs.unlinkSync(`${path_result}merger-hal-2-up.pdf`) // delete files
                    // fs.unlinkSync(`${path_result}merged-fix.pdf`) // delete files
                    // console.log(pdfBytes)
                }
            }
            await excelRekapMerger.generateRekap(listRekap)
        } catch (error) {
            await excelRekapMerger.generateRekap(listRekap)
            // console.log(listRekap)
            console.log(error);
        }
    })();
    res.json({
        message: 'oke pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});
// GENERATE MEGAJIWA ALL
router.get('/posmegajiwa-rekap-merger', async (req, res) => {
    let data = [
        {
            filename: 'file1',
            amplop: 23,
            kertas: 24
        },
        {
            filename: 'file2',
            amplop: 23,
            kertas: 24
        },
        {
            filename: 'file3',
            amplop: 23,
            kertas: 24
        },
        {
            filename: 'file4',
            amplop: 23,
            kertas: 24
        },
        {
            filename: 'file1',
            amplop: 23,
            kertas: 24
        },
        {
            filename: 'file5',
            amplop: 23,
            kertas: 24
        },
    ]
    await excelRekapMerger.generateRekap(data)

    res.json({
        message: 'oke rekap merger pos megajiwa',
        // data: dataInit.jangan_dicetak
    });
});

module.exports = router;

async function addTextPdf(req, path_result) {
    const data = fs.readFileSync(`${path_result}merged-fix.pdf`)
    // const existingPdfBytes = await fetch(`https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf`).then(res => res.arrayBuffer())
    // let bytes = new Uint8Array(existingPdfBytes);
    const pdfDoc = await PDFDocument.load(data)
    // Embed the Helvetica font
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
    // Get the first page of the document
    const pages = pdfDoc.getPages()
    // const firstPage = pages[0]
    // console.log(pages.length)
    // Get the width and height of the first page
//         const { width, height } = firstPage.getSize()

// Draw a string of text diagonally across the first page
    for await (const [i, el] of pages.entries()) {
        el.drawText((i + 1).toString().padStart(4, "0"), {
            x: 45,
            // y: height / 2,
            y: 610,
            size: 7,
            font: helveticaFont,
            // color: rgb(0.95, 0.1, 0.1),
            rotate: degrees(90),
        })
    }
// Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfBytes = await pdfDoc.save()
    fs.writeFileSync(`${path_result}` + req.params.dir + req.params.date + '.pdf', pdfBytes)
    // fs.unlinkSync(`${path_result}merger-hal-1.pdf`)
    // fs.unlinkSync(`${path_result}merger-hal-2-up.pdf`)
    // fs.unlinkSync(`${path_result}merged-fix.pdf`)
//         console.log(pdfBytes)
}
