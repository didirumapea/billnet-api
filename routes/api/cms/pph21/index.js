const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const PDFDocument = require('pdfkit');
const cfg = require('../../../../config');
const fs = require('fs');
const readline = require('readline');
const mkdirp = require('mkdirp');
const { promisify } = require('util');
const readdir = promisify(require('fs').readdir);
const stat = promisify(require('fs').stat);
const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
// const slugify = require('slugify')
const changeCase = require('change-case');
const curFormat = require('../../../../plugins/currency-format')
const data_ptkp = require('../../../../public/data/pph21/ptkp');


router.get('/', (req, res) => {
    res.send('We Are In PPH21 Route')
})


// PERHITUNGAN PTKP
router.get('/pkp=:pkp', async (req, res) => {

    // let ptkp = 260000000; //
    // PERHITUNGAN PKP
    // let ptkp = req.params.pkp; //


    // PERHITUNGAN PTKP

    // console.log(files)
    res.json({
        success: true,
        message: "Perhitungan pkp ok",
        pajak_pph21_setahun_with_cur: curFormat.currencyFormat(calc_tarif_progresif(req.params.pkp), 'Rp '),
        pajak_pph21_setahun: calc_tarif_progresif(req.params.pkp)
    });
});

// PERHITUNGAN PTKP
router.get('/ptkp', async (req, res) => {
    const tagihan_bpjs_kes = await bpjs_tagihan_kes()
    const filename = '/Users/didi-rim-01/Downloads/format-pph21-new.xlsx';
    // const filename = '/Users/didi-rim-01/Downloads/Checklist-Link-backup-H2H-Billnet.xlsx';
    const list = [];
    const all_gross_income = [];
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile(filename)
        .then(function() {
            const worksheet = workbook.worksheets[0]
            worksheet.eachRow(function(row, rowNumber) {
                if (rowNumber >= 10 && Object.keys(row.values).length === 30){
                    // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                    // console.log(Object.keys(row.values).length)
                    // console.log(tagihan_bpjs_kes.find(x => x.name === row.values[2]))
                    const data = {
                        name: row.values[2],
                        job_title: row.values[3],
                        nik: row.values[4],
                        npwp: formatNpwp(row.values[5].toString()),
                        gender: row.values[7],
                        status: row.values[8],
                        upah: {
                            bpjs_ten_ker: row.values[13],
                            bpjs_kes: row.values[14]
                        },
                        // INCOME PER MONTH
                        income_per_month: {
                            penghasilan_teratur: {
                                salary: row.values[17],
                                tunj_mkn_trp: row.values[18].result,
                                overtime: row.values[19],
                                tunj_bpjs: ((row.values[30].result === undefined ? 0 : row.values[30].result) === 0 ? (row.values[14] * 0.01) : parseInt(((row.values[13] * 0.01 >= 85124 ? 85124 : row.values[13] * 0.01) + (row.values[14] * 0.01)).toFixed())),
                                tunj_lain_Lain: row.values[21],
                                jkk_jkm: row.values[22].result === undefined ? 0 : Math.round(row.values[22].result), //jkk+jkm (0,54%)
                                bpjs_kes: row.values[23].result === undefined ? 0 : Math.round(row.values[23].result), // bpjs kes (4%)
                                tunj_pph21: row.values[24], // bpjs kes (4%)
                            },
                            penghasilan_tdk_teratur: {
                                bonus_thr: row.values[25],
                                other_income: row.values[26],
                                tunj_pph21: row.values[27],
                            },
                            ph_bruto: parseInt((row.values[17] + row.values[18].result + row.values[19] + ((row.values[30].result === undefined ? 0 : row.values[30].result) === 0 ? (row.values[14] * 0.01) : (row.values[13] * 0.01 >= 85124 ? 85124 : row.values[13] * 0.01)) + (row.values[14] * 0.01) + row.values[21] + (row.values[22].result === undefined ? 0 : row.values[22].result) + (row.values[23].result === undefined ? 0 : parseInt(row.values[23].result.toFixed())) + row.values[24] + row.values[25] + row.values[26] + row.values[27])),
                        },
                        income_per_year: {
                            penghasilan_teratur: {
                                salary: row.values[17] * 12,
                                tunj_mkn_trp: row.values[18].result  * 12,
                                tunj_bpjs: Math.round(((row.values[30].result === undefined ? 0 : row.values[30].result) === 0 ? (row.values[14] * 0.01) : (row.values[13] * 0.01 >= 85124 ? 85124 : row.values[13] * 0.01) + (row.values[14] * 0.01)) * 12),
                                jkk_jkm: row.values[22].result === undefined ? 0 : Math.round(row.values[22].result * 12), //jkk+jkm (0,54%)
                                bpjs_kes: row.values[23].result === undefined ? 0 : Math.round(row.values[23].result * 12), // bpjs kes (4%)
                                tunj_pph21: row.values[24] * 12, // bpjs kes (4%)
                            },
                            penghasilan_tdk_teratur: {
                                bonus_thr: row.values[25],
                                other_income: row.values[26],
                                tunj_pph21: row.values[27],
                            },
                            ph_bruto: 0
                            // ph_bruto: ((row.values[17] * 12) + (row.values[18].result * 12) + ((row.values[30].result === undefined ? 0 : row.values[30].result) === 0 ? Math.round(row.values[14] * 0.01) : (row.values[13] * 0.01 >= 85124 ? 85124 : Math.round(row.values[13] * 0.01)) + Math.round(row.values[14] * 0.01)) * 12) + ((row.values[22].result === undefined ? 0 : row.values[22].result) * 12) + ((row.values[23].result === undefined ? 0 : row.values[23].result) * 12) + (row.values[24] * 12) + (row.values[25] * 12),
                        },
                        potongan: {
                            JHT: row.values[29].result === undefined ? 0 : row.values[29].result, // JHT (2%)
                            JP: row.values[30].result === undefined ? 0 : row.values[30].result, // JP (1%)
                        },
                        ptkp: row.values[7] !== 'P' ? data_ptkp.find(x => x.code === row.values[8]).value : data_ptkp.find(x => x.code === 'TK0').value,
                        // gross_income: {
                        //     salary: row.values[17] + row.values[18].result + row.values[19] + (row.values[14] * 0.01) + row.values[21] + row.values[24] + row.values[25] + row.values[26] + row.values[27],
                        //     JHT: '',
                        //     JP: row.values[13] * 0.01 >= 85124 ? 85124 : row.values[13] * 0.01,
                        //     KES: row.values[14] * 0.01
                        // }
                    }
                    // BRUTO PER YEAR
                    data.income_per_year.ph_bruto = Object.values(data.income_per_year.penghasilan_teratur).reduce((a, b) => a + b, 0) + Object.values(data.income_per_year.penghasilan_tdk_teratur).reduce((a, b) => a + b, 0);
                    // console.log(Object.values(data.income_per_year.penghasilan_teratur).reduce((a, b) => a + b, 0) + Object.values(data.income_per_year.penghasilan_tdk_teratur).reduce((a, b) => a + b, 0))
                    // CALC PENGURANG
                    data.pengurang =  {
                        biaya_jabatan: {
                            teratur: data.income_per_year.ph_bruto * 0.05 >= 6000000 ? 6000000 : Math.round(data.income_per_year.ph_bruto * 0.05),
                            tdk_teratur: data.income_per_month.penghasilan_tdk_teratur.bonus_thr + data.income_per_month.penghasilan_tdk_teratur.other_income + data.income_per_month.penghasilan_tdk_teratur.tunj_pph21
                        },
                        bpjs: {
                            JHT : row.values[29].result === undefined ? 0 : row.values[29].result * 12, // (2%)
                            JP: row.values[30].result === undefined ? 0 :row.values[30].result * 12, // (1%)
                        }
                    }
                    data.pengurang.biaya_jabatan.tdk_teratur = data.pengurang.biaya_jabatan.tdk_teratur >= 6000000 ? 6000000 : data.pengurang.biaya_jabatan.tdk_teratur;
                    data.pengurang.biaya_jabatan.teratur -= data.pengurang.biaya_jabatan.tdk_teratur;

                    // CALC PENGHASILAN NETTO
                    data.penghasilan_netto = {
                        teratur: Math.round(data.income_per_year.ph_bruto - data.pengurang.biaya_jabatan.teratur - data.pengurang.biaya_jabatan.tdk_teratur - data.pengurang.bpjs.JHT - data.pengurang.bpjs.JP),
                        tdk_teratur: (data.income_per_year.penghasilan_tdk_teratur.bonus_thr + data.income_per_year.penghasilan_tdk_teratur.other_income + data.income_per_year.penghasilan_tdk_teratur.tunj_pph21) - data.pengurang.biaya_jabatan.tdk_teratur,
                        sebelumnya: 0, // NEXT STEP
                        jumlah: 0
                    }

                    data.penghasilan_netto.jumlah = data.penghasilan_netto.teratur + data.penghasilan_netto.tdk_teratur;
                    // CALC PKP
                    data.pkp = {
                        teratur: data.penghasilan_netto.jumlah - data.ptkp < 0 ? 0 : Math.floor(((data.penghasilan_netto.jumlah - data.ptkp)/1000))*1000,
                        tdk_teratur: 0, // SEBELUMNYA
                        jumlah: data.penghasilan_netto.jumlah - data.ptkp < 0 ? 0 : Math.floor(((data.penghasilan_netto.jumlah - data.ptkp)/1000))*1000,
                    }
                    data.pph21_per_year = {
                        penghasilan: {
                            teratur: calc_tarif_progresif(data.pkp.jumlah),
                            tdk_teratur: 0
                        },
                        sebelumnya : 0,
                        jumlah: calc_tarif_progresif(data.pkp.jumlah)
                    }
                    data.pph21_telah_disetor = {
                        penghasilan: {
                            teratur: 0,
                            tdk_teratur: 0
                        }
                    }
                    data.pph21_per_month = {
                        penghasilan: {
                            teratur: Math.round(data.pph21_per_year.penghasilan.teratur / 12),
                            tdk_teratur: 0
                        }
                    }
                    data.kenaikan = {
                        non_npwp: data.npwp.isTrue ? 0 : Math.round((data.pph21_per_month.penghasilan.teratur + data.pph21_per_month.penghasilan.tdk_teratur) * 0.2) // NON NPWP (20%)
                    }
                    data.pjk_yg_hrs_dbyr = data.pph21_per_month.penghasilan.teratur + data.pph21_per_month.penghasilan.tdk_teratur
                    // console.log(Math.floor(data.pkp.teratur/1000)*1000) // CALC MATH FLOOR
                    // LAST RESULT
                    const data2 = {
                        gross_income: {
                            salary: (row.values[17] + row.values[18].result + row.values[19] + data.income_per_month.penghasilan_teratur.tunj_bpjs + row.values[21] + row.values[24] + row.values[25] + row.values[26] + row.values[27]),
                        },
                        deduction: {
                            JHT: Math.round(row.values[13] * 0.02),
                            JP: data.potongan.JP, //row.values[13] * 0.01 >= 85124 ? 85124 : Math.round(row.values[13] * 0.01),
                            KES: Math.round(row.values[14] * 0.01),
                            KES_FAMILY : tagihan_bpjs_kes.find(x => x.name === row.values[2]) !== undefined ? tagihan_bpjs_kes.find(x => x.name === row.values[2]).premi : 0, // tambahan keluarga bpjs kes // DATA PISAH
                            pajak_pph21: data.pjk_yg_hrs_dbyr,
                            pulsa: 0, // TSEL // DATA PISAH
                            pnjmn_karyawan: 0, // DATA PISAH
                            lain2: 0, // DATA PISAH
                            uang_pesangon: 0, // DATA PISAH
                        }
                    }
                    data2.deduction.take_home_pay = Math.floor(data2.gross_income.salary - Object.values(data2.deduction).reduce((a,b) => a + b, 0)) // DATA PISAH
                    // console.log(data2.deduction.take_home_pay)
                    // console.log(data.potongan.JP, row.values[13] * 0.01 >= 85124 ? 85124 : Math.round(row.values[13] * 0.01))
                    // console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values));
                    // console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values[3]));
                    list.push(data);
                    all_gross_income.push(data2)
                }
            });
            // worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
            //     console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
            // });
        });

    // console.log(list)
    res.json({
        success: true,
        message: "ptkp ok",
        data: list,
        result: all_gross_income
    })
});

// PERHITUNGAN PTKP
router.get('/tagihan-bpjs-kes', async (req, res) => {
    res.json({
        success: true,
        message: 'message',
        data: await bpjs_tagihan_kes()
    });
});

// PERHITUNGAN PTKP
router.get('/slip-gaji', async (req, res) => {
    const doc = new PDFDocument({
        size: 'A4',
        bufferPages: true
    });
    doc.pipe(fs.createWriteStream('/mnt/cdn/billnet-files/assets/files/pph21/test.pdf')); // write to PDF
    doc.registerFont('rock', 'public/fonts/pph21/rock.ttf');
    doc.registerFont('rock-bold', 'public/fonts/pph21/rockb.ttf');
    doc.registerFont('arial_bold', 'public/fonts/pph21/arialbd_0.ttf');
    doc.registerFont('arial', 'public/fonts/bank-mega/arial_0.ttf');
    // pdfDoc.pipe(res);                                       // HTTP response

    // IMAGES LOGO
    // Scale proprotionally to the specified width
    doc.image('public/images/pph21/logo/griya-trada.png', 410, 35, {width: 150})

    doc
        //
        // .fillColor('black')
        .font('rock-bold')
        .fontSize(19)
        // TITLE
        .text('SLIP GAJI KARYAWAN', 28, 65, {
            width: 400,
            align: 'left',
            underline: true
        })
        // PERIODE
        .font('Courier-Bold')
        .fontSize(8)
        .text('Periode', 28, 100)
        .text(': JAN - 2020', 165, 100)
        // Title
        .font('Courier')
        .text('Nama', 28, 115)
        .text('NPWP', 28, 125)
        .text('Jabatan', 28, 135)
        .text('Jenis Kelamin', 28, 145)
        .text('Status Pajak', 28, 155)
        // Title Values
        .text(': Nindya Pramesti Rengganis Rumapea', 165, 115)
        .text(': 23.521.215-1.162.665', 165, 125)
        .text(': Tax Accounting', 165, 135)
        .text(': P', 165, 145)
        .text(': K1', 165, 155)

    // LINE
    doc
        // long line
        .moveTo(28, 185)                               // set the current point
        .lineTo(doc.page.width - 28, 185)
        // line head 1
        .moveTo(28, 200)                               // set the current point
        .lineTo(310, 200)
        // text
        .font('Courier-Bold')
        .fontSize(9)
        .text('PENGHASILAN', 28, 189, {
            width: 285,
            align: 'center',
        })
        // line head 2
        .moveTo(350, 200)                               // set the current point
        .lineTo(doc.page.width - 28, 200)
        //text
        .text('POTONGAN', 350, 189, {
            width: 220,
            align: 'center',
        })
        .lineWidth(0.75)
        .stroke();


    doc
        .font('Courier')
        .fontSize(8)
        // BODY
        .text('Gaji Pokok', 28, 215)
        .text('Tunj. Jabatan', 28, 225)
        .text('Tunj. Makan & Transport', 28, 235)
        .text('Tunj. Overtime', 28, 245)
        .text('Tunj. Lain-lain', 28, 255)
        .text('Tunj. Pajak', 28, 265)
        .text('Bonus', 28, 275)
        .text('THR', 28, 285)
        .text('Tunj. Pajak THR/Bonus', 28, 295)
        //
        .font('Courier-Bold')
        .text('Total Penghasilan', 28, 315)
        .text('Penghasilan yang diterima', 28, 335)
        //
        .font('Courier')
        .text('Terbilang', 28, 355)
        // DATA PREMI BPJS
        .font('Courier-Bold')
        .text('DATA PREMI BPJS :', 28, 385)
        .font('Courier')
        .text('JKK + JKM (0,54%)', 28, 395)
        .text('JHT (3,7%)', 28, 405)
        .text('JP (2%)', 28, 415)
        .text('BPJS Kes (4%)', 28, 425)
        // CATATAN
        .font('arial_bold')
        .fontSize(5)
        .text('CATATAN : ', 28, 533)
        .font('arial')
        .text('P : Perempuan / L : Laki-laki : ', 28, 543)
        .text('TK0 : Belum Menikah : ', 28, 553)
        .text('K0 : Menikah Belum Mempunyai anak : ', 28, 563)
        .text('K1 : Menikah Anak-1', 170, 543)
        .text('K2 : Menikah Anak-2', 170, 553)
        .text('K3 : Menikah Anak-3', 170, 563)
        // LINE TOTAL
        .moveTo(175, 313)                               // set the current point
        .lineTo(310, 313)
        //
        .moveTo(175, 323)                               // set the current point
        .lineTo(310, 323)
        //
        .moveTo(175, 325)                               // set the current point
        .lineTo(310, 325)
        //
        .lineWidth(0.75)
        .stroke()
        //
        .font('Courier')
        .fontSize(8)
        .text(':', 165, 215)
        .text(':', 165, 225)
        .text(':', 165, 235)
        .text(':', 165, 245)
        .text(':', 165, 255)
        .text(':', 165, 265)
        .text(':', 165, 275)
        .text(':', 165, 285)
        .text(':', 165, 295)
        .text(':', 165, 355)
        .text(':', 165, 395)
        .text(':', 165, 405)
        .text(':', 165, 415)
        .text(':', 165, 425)
        // BODY VALUE
        .text(`2.887.500,00`, 165, 215,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 225,  {
            width: 145,
            align: 'right',
        })
        .text(`1.300.000,00`, 165, 235,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 245,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 255,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 265,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 275,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 285,  {
            width: 145,
            align: 'right',
        })
        .text(`0,00`, 165, 295,  {
            width: 145,
            align: 'right',
        })
        .font('Courier-Bold')
        .text(`4.187.500,00`, 165, 315,  {
            width: 145,
            align: 'right',
        })
        .text('Rp', 180, 335)
        .text(`4.029.860,00`, 160, 335,  {
            width: 145,
            align: 'right',
        })
        .text(`Empat juta dua puluh sembilan ribu delapan ratus enam puluh rupiah`, 180, 355,  {
            width: doc.page.width - 28 - 175,
            align: 'left',
        })
        .rect(175, 333, 135, 10) // rect total penghasilan
        .rect(175, 353, doc.page.width - 28 - 175, 20) // rect terbilang
        // PREMI BPJS
        .font('Courier')
        .text(`21.281,29`, 165, 395,  {
            width: 145,
            align: 'right',
        })
        .text(`145.816,26`, 165, 405,  {
            width: 145,
            align: 'right',
        })
        .text(`78.819,60`, 165, 415,  {
            width: 145,
            align: 'right',
        })
        .text(`157.639,20`, 165, 425,  {
            width: 145,
            align: 'right',
        })
        // SIGNATURE AREA
        .text(`Penerima,`, 75, 455)
        .text(`Jakarta, 31 Januari 2020`, 360, 455)
        .text(`HERMAN`, 28, 505, {
            width: 130,
            align: 'center',
        })
        .text(`..........`, 360, 505, {
            width: 110,
            align: 'center',
        })
        .fontSize(7.5)
        .text(`Employee`, 28, 513, {
            width: 130,
            align: 'center',
        })
        .text(`Human Resources`, 360, 513, {
            width: 110,
            align: 'center',
        })
        // LINE TOTAL
        .moveTo(480, 313)                               // set the current point
        .lineTo(doc.page.width - 28, 313)
        //
        .moveTo(480, 323)                               // set the current point
        .lineTo(doc.page.width - 28, 323)
        //
        .moveTo(480, 325)                               // set the current point
        .lineTo(doc.page.width - 28, 325)
        //
        .moveTo(28, 512)                               // SIGNATURE LINE LEFT
        .lineTo(165, 512)
        //
        .moveTo(360, 512)                               // SIGNATURE LINE RIGHT
        .lineTo(475, 512)
        //
        .lineWidth(0.75)
        .stroke();

    doc
        // BODY 2
        .font('Courier')
        .fontSize(8)
        .text('BPJS TK JHT (2%)', 360, 215)
        .text('BPJS TK JP (1%)', 360, 225)
        .text('BPJS KES (1%)', 360, 235)
        .text('Pajak', 360, 245)
        .text('Pulsa', 360, 255)
        .text('Pinjaman', 360, 265)
        .text('Lain-lain', 360, 275)
        //
        .font('Courier-Bold')
        .text('Total Potongan', 360, 315)
        .text(':', 475, 215)
        .text(':', 475, 225)
        .text(':', 475, 235)
        .text(':', 475, 245)
        .text(':', 475, 255)
        .text(':', 475, 265)
        .text(':', 475, 275)
        .text(`78.819,60`, 475, 215,  {
            width: 90,
            align: 'right',
        })
        .text(`39.409,80`, 475, 225,  {
            width: 90,
            align: 'right',
        })
        .text(`39.409,80`, 475, 235,  {
            width: 90,
            align: 'right',
        })
        .text(`0,00`, 475, 245,  {
            width: 90,
            align: 'right',
        })
        .text(`0,00`, 475, 255,  {
            width: 90,
            align: 'right',
        })
        .text(`0,00`, 475, 265,  {
            width: 90,
            align: 'right',
        })
        .text(`0,00`, 475, 275,  {
            width: 90,
            align: 'right',
        })
        .font('Courier-Bold')
        .text(`157.639,20`, 475, 315,  {
            width: 90,
            align: 'right',
        });
// add stuff to PDF here using methods described below...

// finalize the PDF and end the stream
    doc.end();
    // console.log(files)
    res.json({
        success: true,
        message: "Slip Gaji ok",
    });
});


module.exports = router;

function formatNpwp(value) {
    value = value.length <= 14 ? '0' + value : value
    if (typeof value === 'string') {
        const parse = value.replace(/(\d{2})(\d{3})(\d{3})(\d{1})(\d{3})(\d{3})/, '$1.$2.$3.$4-$5.$6')
        return {
            val : parse,
            isTrue : parse !== '00.000.000.0-000.000'
        } ;
    }
}

function calc_tarif_progresif(pkp){
    const data_pkp = [
        {
            title: 't1',
            range: '0 - 50jt',
            percentage: 0.05,
            value: 50000000,
            start: 0,
            end : 50000000,
        },
        {
            title: 't2',
            range: '50jt - 250jt',
            percentage: 0.15,
            value: 200000000,
            start: 50000001,
            end : 200000000,
        },
        {
            title: 't3',
            range: '250jt - 500jt',
            percentage: 0.25,
            value: 250000000,
            start: 250000001,
            end : 500000000,
        },
        {
            title: 't4',
            range: '500jt ++++',
            percentage: 0.3,
            value: 500000000,
            start: 500000001,
            end : 'EON',
        }
    ];
    let result_pkp = 0;
    let total_pph21_setahun = 0;
    let sisa_pkp = pkp;

    for (let el of data_pkp) {
        // console.log('sisa ptkp : '+ sisa_ptkp)
        if (sisa_pkp > el.value){
            result_pkp = el.value
            sisa_pkp -= el.value;
        }
        else{
            result_pkp = sisa_pkp
            sisa_pkp = sisa_pkp - el.value < 0 ? 0 : sisa_pkp -= el.value
        }
        // result_ptkp = el.value > ptkp ? sisa_ptkp : sisa_ptkp - el.value
        total_pph21_setahun += result_pkp * el.percentage;
        // console.log('total nilai pph21 setahun : '+ total_pph21_setahun)
        if (el.start > pkp && (el.end < pkp || el.end === 'EON')){
            break;
        }
    }
    return total_pph21_setahun
}

async function bpjs_tagihan_kes(){
    const filename = '/Users/didi-rim-01/Didi/project/PPH21/Tagihan01123082202001.xlsx';
    const workbook = new ExcelJS.Workbook();
    const tmbhn_kel_bpjs = [];
    return await workbook.xlsx.readFile(filename)
        .then(async function () {
            let message = 'Data not found.'
            if (workbook.worksheets.length > 1) {
                const worksheet = workbook.worksheets[1]
                const worksheet2 = workbook.worksheets[0]
                message = 'Checking data successfully.';
                // console.log(workbook.worksheets.length)
                await worksheet.eachRow({includeEmpty: true}, async function (row, rowNumber) {
                    if (rowNumber > 7 && row.values[1] !== 'TOTAL PREMI') {
                        // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                        // console.log(row.values[1]) // no jkn pekerja
                        await worksheet2.eachRow({includeEmpty: true}, function (row2, rowNumber2) {
                            if (rowNumber2 > 7 && row2.values[2] !== 'TOTAL PREMI') {
                                // console.log(row.values[1], row2.values[2]) // no jkn peserta
                                if (row.values[1] === row2.values[2]) {
                                    let data = {
                                        name: row2.values[3],
                                        no_jkn: row2.values[2],
                                        premi: row.values[6]
                                    }
                                    tmbhn_kel_bpjs.push(data);
                                    // console.log("Row " + rowNumber2 + " = " + JSON.stringify(row2.values));
                                    // console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
                                    // console.log(row2.values[2]) // no jkn pekerja
                                }

                            }
                        });
                    }
                });
            }
            // console.log(files)
            return tmbhn_kel_bpjs
        });
    // console.log(data)
}
