const express = require('express');
const router = express.Router();
const checkAuthEmployee = require('../../middleware/check-auth-employees')
const checkAuthAdmin = require('../../middleware/check-auth-admin')
const url1 = '/apps';
const url2 = '/cms';
const url3 = '/web';


// region CMS
// ------------------------------------------------ CMS -----
// PATH FOLDER
// GENERATE
const cms_generate = require('./cms/generate');
// GENERATE-MERGER
const cms_generate_merger = require('./cms/generate/merger');
// EMAIL
const cms_email = require('./cms/email');
// E-VOUCHER
const cms_voucher = require('./cms/e-voucher');
// E-PROMO
const cms_promo = require('./cms/e-promo');
// AUTH
const cms_auth = require('./cms/auth');
// PPH21
const cms_pph21 = require('./cms/pph21');


// PATH URL/API
// GENERATE
router.use(`${url2}/generate`, cms_generate);
// GENERATE-MERGER
router.use(`${url2}/generate/merger`, cms_generate_merger);
// EMAIL
router.use(`${url2}/email`, cms_email);
// E-PROMO
router.use(`${url2}/e-promo`, cms_promo);
// E-VOUCHER
router.use(`${url2}/e-voucher`, cms_voucher);
// AUTH
router.use(`${url2}/auth`, cms_auth);
// PPH21
router.use(`${url2}/pph21`, cms_pph21);
//endregion


// region APPS
// --- -------------------------------------------- APPS -----------------------------------------------
// PATH FOLDER
// PAYROLL
const apps_payroll = require('./apps/payroll');
// AUTH
const apps_auth = require('./apps/auth');
const apps_auth_employees = require('./apps/auth/employees');
// EMPLOYEES
const apps_employees = require('./apps/employees');

// PATH URL / API
// PAYROLL
router.use(`${url1}/payroll`, apps_payroll);
//region AUTH
router.use(`${url1}/auth`, apps_auth);
router.use(`${url1}/auth/employees`, apps_auth_employees);
// EMPLOYEES
router.use(`${url1}/employees`, checkAuthEmployee, apps_employees);

// endregion

// region APPS
// --- -------------------------------------------- HOME -----------------------------------------------
// PATH FOLDER
// HOME
const web_home = require('./web/home');

// PATH URL / API
// HOME
router.use(`${url3}/home`, web_home);

// endregion

module.exports = router;