const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('master_organization').del()
        .then(function () {
            // Inserts seed entries
            return knex('master_organization').insert([
                {
                    id: 1,
                    id_account_type: 1,
                    organization_code: 'general-001',
                    name: 'General',
                    email: 'gen@gmail.com',
                    slug: 'general',
                    city_id: 1101,
                    province_id: 11,
                    img_url: 'organisasi-2.png',
                    status_email_verification: '1',
                    is_verified: '1'
                },
                {
                    id: 2,
                    id_account_type: 3,
                    organization_code: 'bnpb-001',
                    name: 'BNPB',
                    email: 'bnpb@gmail.com',
                    slug: 'bnpb',
                    city_id: 1101,
                    province_id: 11,
                    img_url: 'organisasi-2.png',
                    status_email_verification: '1',
                    is_verified: '1'
                },
                {
                    id: 3,
                    id_account_type: 3,
                    organization_code: 'bmkg-001',
                    name: 'BMKG',
                    email: 'bmkg@gmail.com',
                    slug: 'bmkg',
                    city_id: 1101,
                    province_id: 11,
                    img_url: 'organisasi-2.png',
                    status_email_verification: '1',
                    is_verified: '1'
                },
                {
                    id: 4,
                    id_account_type: 2,
                    organization_code: 'stc-001',
                    name: 'STC',
                    email: 'stc@gmail.com',
                    slug: 'stc',
                    city_id: 1101,
                    province_id: 11,
                    img_url: 'organisasi-2.png',
                    status_email_verification: '1',
                    is_verified: '1'
                },
                {
                    id: 5,
                    id_account_type: 5,
                    organization_code: 'adm-bumikita-001',
                    name: 'Admin Bumikita',
                    email: 'office-bumikita@gmail.com',
                    slug: 'admin-bumikita',
                    city_id: 1101,
                    province_id: 11,
                    img_url: 'organisasi-2.png',
                    status_email_verification: '1',
                    is_verified: '1'
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
