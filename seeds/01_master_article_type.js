// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('master_article_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('master_article_type').insert([
                {
                    id: 1,
                    name: 'Cerita Warga',
                    slug: 'cerita-warga',
                    color: '222313',
                },
                {   id: 2,
                    name: 'Public',
                    slug: 'public',
                    color: '23315H',
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
