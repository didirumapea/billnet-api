// const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('master_disaster').del()
        .then(function () {
            // Inserts seed entries
            return knex('master_disaster').insert([
                {
                    id: 1,
                    name: 'Gempa Bumi',
                    slug: 'gempa-bumi',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_11@3x.png'
                },
                {
                    id: 2,
                    name: 'Banjir',
                    slug: 'banjir',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_8@3x.png'
                },
                {
                    id: 3,
                    name: 'Longsor',
                    slug: 'longsor',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_3@3x.png'
                },
                {
                    id: 4,
                    name: 'Kekeringan',
                    slug: 'kekeringan',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_10@3x.png'
                },
                {
                    id: 5,
                    name: 'Badai',
                    slug: 'badai',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_9@3x.png'
                },
                {
                    id: 6,
                    name: 'Tsunami',
                    slug: 'tsunami',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_7@3x.png'
                },
                {
                    id: 7,
                    name: 'Angin Topan',
                    slug: 'angin-topan',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_6@3x.png'
                },
                {
                    id: 8,
                    name: 'Kebakaran Hutan',
                    slug: 'kebakaran-hutan',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_5@3x.png'
                },
                {
                    id: 9,
                    name: 'Polusi',
                    slug: 'polusi',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_2@3x.png'
                },
                {
                    id: 10,
                    name: 'Wabah',
                    slug: 'wabah',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_1@3x.png'
                },
                {
                    id: 11,
                    name: 'Meteor',
                    slug: 'meteor',
                    exclude_province_area: `{"name": [1, 2, 3, 4]}`,
                    image_url: 'icons_4@3x.png'
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
