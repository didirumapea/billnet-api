const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('report_disaster_org').del()
        .then(function () {
            // Inserts seed entries
            return knex('report_disaster_org').insert([
                {
                    id: 1,
                    user_id: 1,
                    organization_id: 1,
                    disaster_id: 1,
                    report_unique_id: 'dsa9s9tmb1',
                    title: 'This is First Title report disaster org',
                    slug: 'this-is-first-title-report-disaster-org',
                    desc: 'This is First Desc',
                    nama_lokasi: 'Jati Nangor',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    disaster_radius: 5000,
                    disaster_status: 'Siaga',
                    rt_rw: '02/08',
                    dusun: 'Bojong Gede',
                    kelurahan: 'Kedung Waringin',
                    kecamatan: 'Bojong Gede',
                    city_id: 1101,
                    province_id: 11,
                    meninggal_laki2: 54,
                    meninggal_perempuan: 12,
                    sakit_cidera_laki2: 87,
                    sakit_cidera_perempuan: 23,
                    terdampak_laki2: 64,
                    terdampak_perempuan: 84,
                    terdampak_anak_laki2: 31,
                    terdampak_anak_perempuan: 18,
                    terdampak_disabilitas_laki2: 122,
                    terdampak_disabilitas_perempuan: 2,
                    jumlah_jiwa_terdampak: 32,
                    jumlah_kk_terdampak: 4,
                    kerusakan_dan_jumlah_kerusakan: 52,
                    kebutuhan_cepat: 'sandang, pangan, papan',
                    publish_date: '2020-03-15 19:34:16',
                },
                {
                    id: 2,
                    user_id: 2,
                    organization_id: 2,
                    disaster_id: 3,
                    report_unique_id: 'DSJ92LG8W5',
                    title: 'This is Second Title report disaster org',
                    slug: 'this-is-second-title-report-disaster-org',
                    desc: 'This is First Desc',
                    nama_lokasi: 'Matraman',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    disaster_radius: 2245,
                    disaster_status: 'Waspada',
                    rt_rw: '07/08',
                    dusun: 'Bojong Depok Baru 2',
                    kelurahan: 'Kedung Waringin',
                    kecamatan: 'Bojong Gede',
                    city_id: 1101,
                    province_id: 11,
                    meninggal_laki2: 82,
                    meninggal_perempuan: 32,
                    sakit_cidera_laki2: 12,
                    sakit_cidera_perempuan: 55,
                    terdampak_laki2: 12,
                    terdampak_perempuan: 3,
                    terdampak_anak_laki2: 25,
                    terdampak_anak_perempuan: 77,
                    terdampak_disabilitas_laki2: 12,
                    terdampak_disabilitas_perempuan: 1,
                    jumlah_jiwa_terdampak: 42,
                    jumlah_kk_terdampak: 2,
                    kerusakan_dan_jumlah_kerusakan: 12,
                    kebutuhan_cepat: 'sarung, selimut, makanan pokok',
                    publish_date: '2020-03-15 19:34:16',
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
