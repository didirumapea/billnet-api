const date = require('../plugins/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('content').del()
        .then(function () {
            // Inserts seed entries
            return knex('content').insert([
                {
                    user_id: 1,
                    content_type_id: 1,
                    disaster_id: 1,
                    article_type_id: 1,
                    title: 'This is First Content',
                    slug: 'this-is-first-content',
                    content: 'This is First Desc',
                    image: 'image_first_content.jpg',
                    publish_date: '2020-03-15 19:34:16',
                },
                {
                    user_id: 1,
                    content_type_id: 2,
                    disaster_id: 2,
                    article_type_id: 2,
                    title: 'This is Second Content',
                    slug: 'this-is-second-content',
                    content: 'This is Second Desc',
                    image: 'image_Second_content.jpg',
                    publish_date: '2020-03-15 19:34:16',
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
